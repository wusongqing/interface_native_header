/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_NATIVE_DISPLAY_INFO_H
#define OH_NATIVE_DISPLAY_INFO_H

/**
 * @addtogroup OH_DisplayManager
 * @{
 *
 * @brief Provides the display management capability.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

/**
 * @file oh_display_info.h
 *
 * @brief Declares the common enums and definitions of the display manager.
 *
 * @kit ArkUI
 * File to include: <window_manager/oh_display_info.h>
 * @library libnative_display_manager.so
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the clockwise rotation angles of a display.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_Rotation {
    /** The display is rotated clockwise by 0 degrees. */
    DISPLAY_MANAGER_ROTATION_0 = 0,

    /** The display is rotated clockwise by 90 degrees. */
    DISPLAY_MANAGER_ROTATION_90 = 1,

    /** The display is rotated clockwise by 180 degrees. */
    DISPLAY_MANAGER_ROTATION_180 = 2,

    /** The display is rotated clockwise by 270 degrees. */
    DISPLAY_MANAGER_ROTATION_270 = 3,
} NativeDisplayManager_Rotation;

/**
 * @brief Enumerates the orientations of a display.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_Orientation {
    /** The display is in portrait mode. */
    DISPLAY_MANAGER_PORTRAIT = 0,

    /** The display is in landscape mode. */
    DISPLAY_MANAGER_LANDSCAPE = 1,

    /** The display is in reverse portrait mode. */
    DISPLAY_MANAGER_PORTRAIT_INVERTED = 2,

    /** The display is in reverse landscape mode. */
    DISPLAY_MANAGER_LANDSCAPE_INVERTED = 3,

    /** The screen orientation is unknown. */
    DISPLAY_MANAGER_UNKNOWN,
} NativeDisplayManager_Orientation;

/**
 * @brief Enumerates the status codes returned by the display manager interface.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_ErrorCode {
    /** The operation is successful. */
    DISPLAY_MANAGER_OK = 0,

    /** Permission verification failed. The application does not have the permission to use the API. */
    DISPLAY_MANAGER_ERROR_NO_PERMISSION = 201,

    /** Permission verification failed. A non-system application attempts to call a system API. */
    DISPLAY_MANAGER_ERROR_NOT_SYSTEM_APP = 202,

    /** Parameter check fails. */
    DISPLAY_MANAGER_ERROR_INVALID_PARAM = 401,

    /** The device does not support the API. */
    DISPLAY_MANAGER_ERROR_DEVICE_NOT_SUPPORTED = 801,

    /** The display is invalid. */
    DISPLAY_MANAGER_ERROR_INVALID_SCREEN = 1400001,

    /** The current operation object does not have the operation permission. */
    DISPLAY_MANAGER_ERROR_INVALID_CALL = 1400002,

    /** The system service is abnormal. */
    DISPLAY_MANAGER_ERROR_SYSTEM_ABNORMAL = 1400003,
} NativeDisplayManager_ErrorCode;

/**
 * @brief Enumerates the display modes of a foldable device.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef enum NativeDisplayManager_FoldDisplayMode {
    /** The display mode of the device is unknown. */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_UNKNOWN = 0,

    /** The device is displayed in full screen. */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_FULL = 1,

    /** The main screen of the device is displayed. */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_MAIN = 2,

    /** The subscreen of the device is displayed. */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_SUB = 3,

    /** Both screens of the device are displayed in collaborative mode. */
    DISPLAY_MANAGER_FOLD_DISPLAY_MODE_COORDINATION = 4,
} NativeDisplayManager_FoldDisplayMode;

/**
 * @brief Describes a rectangle.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_Rect {
    /** Left boundary of the rectangle. */
    int32_t left;

    /** Top boundary of the rectangle. */
    int32_t top;

    /** Width of the rectangle. */
    uint32_t width;

    /** Height of the rectangle. */
    uint32_t height;
} NativeDisplayManager_Rect;

/**
 * @brief Describes the curved area on the waterfall display.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_WaterfallDisplayAreaRects {
    /** Rectangle of the curved area on the left of the waterfall display. */
    NativeDisplayManager_Rect left;

    /** Rectangle of the curved area on the top of the waterfall display. */
    NativeDisplayManager_Rect top;

    /** Rectangle of the curved area on the right of the waterfall display. */
    NativeDisplayManager_Rect right;

    /** Rectangle of the curved area on the bottom of the waterfall display. */
    NativeDisplayManager_Rect bottom;
} NativeDisplayManager_WaterfallDisplayAreaRects;

/**
 * @brief Describes the cutout, which is an area that is not intended for displaying content on the display.
 *
 * @syscap SystemCapability.WindowManager.WindowManager.Core
 * @since 12
 * @version 1.0
 */
typedef struct NativeDisplayManager_CutoutInfo {
    /** Length of the bounding rectangle for punch holes and notches. */
    int32_t boundingRectsLength;

    /** Bounding rectangle for punch holes and notches. */
    NativeDisplayManager_Rect *boundingRects;

    /** Curved area on the waterfall display. */
    NativeDisplayManager_WaterfallDisplayAreaRects waterfallDisplayAreaRects;
} NativeDisplayManager_CutoutInfo;

#ifdef __cplusplus
}
#endif
/** @} */
#endif // OH_NATIVE_DISPLAY_INFO_H
