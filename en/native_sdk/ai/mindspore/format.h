/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 *
 * @brief Provides APIs related to MindSpore Lite model inference.
 *
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file format.h
 *
 * @brief Declares tensor data formats.
 *
 * File to include: \<mindspore/format.h>
 * @library libmindspore_lite_ndk.so
 * @since 9
 */
#ifndef MINDSPORE_INCLUDE_C_API_FORMAT_C_H
#define MINDSPORE_INCLUDE_C_API_FORMAT_C_H

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief Declares data formats supported by MSTensor.
 *
 * @since 9
 */
typedef enum OH_AI_Format {
  /** Tensor data is stored in the sequence of batch number N, channel C, height H, and width W. */
  OH_AI_FORMAT_NCHW = 0,
  /** Tensor data is stored in the sequence of batch number N, height H, width W, and channel C. */
  OH_AI_FORMAT_NHWC = 1,
  /** Tensor data is stored in the sequence of batch number N, height H, width W, and channel C.
      The C axis is 4-byte aligned. */
  OH_AI_FORMAT_NHWC4 = 2,
  /** Tensor data is stored in the sequence of height H, width W, core count K, and channel C. */
  OH_AI_FORMAT_HWKC = 3,
  /** Tensor data is stored in the sequence of height H, width W, channel C, and core count K. */
  OH_AI_FORMAT_HWCK = 4,
  /** Tensor data is stored in the sequence of core count K, channel C, height H, and width W. */
  OH_AI_FORMAT_KCHW = 5,
  /** Tensor data is stored in the sequence of channel C, core count K, height H, and width W. */
  OH_AI_FORMAT_CKHW = 6,
  /** Tensor data is stored in the sequence of core count K, height H, width W, and channel C. */
  OH_AI_FORMAT_KHWC = 7,
  /** Tensor data is stored in the sequence of channel C, height H, width W, and core count K. */
  OH_AI_FORMAT_CHWK = 8,
  /** Tensor data is stored in the sequence of height H and width W. */
  OH_AI_FORMAT_HW = 9,
  /** Tensor data is stored in the sequence of height H and width W. The W axis is 4-byte aligned. */
  OH_AI_FORMAT_HW4 = 10,
  /** Tensor data is stored in the sequence of batch number N and channel C. */
  OH_AI_FORMAT_NC = 11,
  /** Tensor data is stored in the sequence of batch number N and channel C.
      The C axis is 4-byte aligned. */
  OH_AI_FORMAT_NC4 = 12,
  /** Tensor data is stored in the sequence of batch number N, channel C, height H, and width W.
      The C axis and W axis are 4-byte aligned. */
  OH_AI_FORMAT_NC4HW4 = 13,
  /** Tensor data is stored in the sequence of batch number N, channel C, depth D, height H, and width W. */
  OH_AI_FORMAT_NCDHW = 15,
  /** Tensor data is stored in the sequence of batch number N, width W, and channel C. */
  OH_AI_FORMAT_NWC = 16,
  /** Tensor data is stored in the sequence of batch number N, channel C, and width W. */
  OH_AI_FORMAT_NCW = 17
} OH_AI_Format;

#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_FORMAT_C_H
