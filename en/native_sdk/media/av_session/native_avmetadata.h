/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OHAVSession
 * @{
 *
 * @brief Defines the C APIs of the playback control module.
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 *
 * @since 13
 * @version 1.0
 */

/**
 * @file native_avmetadata.h
 *
 * @brief Declares the definitions of playback control metadata.
 *
 * @library libohavsession.so
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @kit AVSessionKit
 * @since 13
 * @version 1.0
 */

#ifndef NATIVE_AVMETADATA_H
#define NATIVE_AVMETADATA_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the error codes related to metadata operations.
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @error Operation successful.
     */
    AVMETADATA_SUCCESS = 0,

    /**
     * @error Incorrect parameter.
     */
    AVMETADATA_ERROR_INVALID_PARAM = 1,

    /**
     * @error Insufficient memory.
     */
    AVMETADATA_ERROR_NO_MEMORY = 2,
} AVMetadata_Result;

/**
 * @brief Enumerates the fast-forward or rewind intervals supported by the AVSession.
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @brief The time is 10 seconds.
     */
    SECONDS_10 = 10,

    /**
     * @brief The time is 15 seconds.
     */
    SECONDS_15 = 15,

    /**
     * @brief The time is 30 seconds.
     */
    SECONDS_30 = 30,
} AVMetadata_SkipIntervals;

/**
 * @brief Enumerates the display tags of the media asset.
 * The display tag is a special type identifier of the media audio source.
 *
 * @since 13
 * @version 1.0
 */
typedef enum {
    /**
     * @brief AUDIO VIVID.
     */
    AVSESSION_DISPLAYTAG_AUDIO_VIVID = 1,
} AVMetadata_DisplayTag;

/**
 * @brief Describes a session metadata builder.
 * The builder is used to construct session metadata.
 *
 * @since 13
 * @version 1.0
 */
typedef struct OH_AVMetadataBuilderStruct OH_AVMetadataBuilder;

/**
 * @brief Describes session metadata.
 * It is an AVMetadata instance set for a media asset.
 *
 * @since 13
 * @version 1.0
 */
typedef struct OH_AVMetadataStruct OH_AVMetadata;

/**
 * @brief Creates a metadata builder.
 *
 * @param builder Double pointer to the builder created.
 * @return Returns any of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if <b>builder</b> is a null pointer.
 *         {@link AVMETADATA_ERROR_NO_MEMORY} If there is no sufficient memory.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_Create(OH_AVMetadataBuilder** builder);

/**
 * @brief Destroys a metadata builder.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if <b>builder</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_Destroy(OH_AVMetadataBuilder* builder);

/**
 * @brief Sets the ID of the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param assetId Pointer to the asset ID.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>assetId</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetAssetId(OH_AVMetadataBuilder* builder, const char* assetId);

/**
 * @brief Sets a title for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param title Pointer to the title.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>title</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetTitle(OH_AVMetadataBuilder* builder, const char* title);

/**
 * @brief Sets an artist for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param artist Pointer to the artist.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>artist</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetArtist(OH_AVMetadataBuilder* builder, const char* artist);

/**
 * @brief Sets an author for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param author Pointer to the author.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>author</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetAuthor(OH_AVMetadataBuilder* builder, const char* author);

/**
 * @brief Sets an album name for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param album Pointer to the album name.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>album</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetAlbum(OH_AVMetadataBuilder* builder, const char* album);

/**
 * @brief Sets a writer for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param writer Pointer to the writer.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>writer</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetWriter(OH_AVMetadataBuilder* builder, const char* writer);

/**
 * @brief Sets a composer for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param composer Pointer to the composer.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>composer</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetComposer(OH_AVMetadataBuilder* builder, const char* composer);

/**
 * @brief Sets the playback duration for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param duration Playback duration, in ms.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if <b>builder</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetDuration(OH_AVMetadataBuilder* builder, int64_t duration);

/**
 * @brief Sets an image for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param mediaImageUri URI of the image.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>mediaImageUri</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetMediaImageUri(OH_AVMetadataBuilder* builder, const char* mediaImageUri);

/**
 * @brief Sets a subtitle for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param subtitle Pointer to the subtitle.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>subtitle</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetSubtitle(OH_AVMetadataBuilder* builder, const char* subtitle);

/**
 * @brief Sets a description for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param description Pointer to the description.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>description</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetDescription(OH_AVMetadataBuilder* builder, const char* description);

/**
 * @brief Sets lyrics for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param lyric Pointer to the lyrics in the lrc format.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                1. <b>builder</b> is a null pointer.
 *                                                 2. <b>lyric</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetLyric(OH_AVMetadataBuilder* builder, const char* lyric);

/**
 * @brief Sets the skip intervals for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param intervals Skip intervals.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>intervals</b> is invalid.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetSkipIntervals(OH_AVMetadataBuilder* builder,
    AVMetadata_SkipIntervals intervals);

/**
 * @brief Sets display tags for the media asset.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param tags Tags of the media asset displayed on the playback control page.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if <b>builder</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_SetDisplayTags(OH_AVMetadataBuilder* builder, int32_t tags);

/**
 * @brief Generate an <b>OH_AVMetadata</b> object.
 *
 * @param builder Pointer to an <b>OH_AVMetadataBuilder</b> instance.
 * @param avMetadata Double pointer to the <b>OH_AVMetadata</b> object created.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_NO_MEMORY} if the memory is insufficient.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if
 *                                                 1. <b>builder</b> is a null pointer.
 *                                                 2. <b>avMetadata</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadataBuilder_GenerateAVMetadata(OH_AVMetadataBuilder* builder,
    OH_AVMetadata** avMetadata);

/**
 * @brief Releases an <b>OH_AVMetadata</b> object.
 *
 * @param avMetadata Pointer to an <b>OH_AVMetadata</b> object.
 * @return Returns either of the following result codes:
 *         {@link AVMETADATA_SUCCESS} if the function is successfully executed.
 *         {@link AVMETADATA_ERROR_INVALID_PARAM} if <b>avMetadata</b> is a null pointer.
 * @since 13
 */
AVMetadata_Result OH_AVMetadata_Destroy(OH_AVMetadata* avMetadata);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_AVMETADATA_H
/** @} */
