/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_SHADER_EFFECT_H
#define C_INCLUDE_DRAWING_SHADER_EFFECT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_shader_effect.h
 *
 * @brief Declares the functions related to the shader effect in the drawing module.
 *
 * File to include: native_drawing/drawing_shader_effect.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the tile modes of the shader effect.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_TileMode {
    /**
     * Replicates the edge color if the shader effect draws outside of its original boundary.
     */
    CLAMP,
    /**
     * Repeats the shader effect's image in both horizontal and vertical directions.
     */
    REPEAT,
    /**
     * Repeats the shader effect's image in both horizontal and vertical directions, alternating mirror images.
     */
    MIRROR,
    /**
     * Renders the shader effect's image only within the original boundary, and returns transparent black elsewhere.
     */
    DECAL,
} OH_Drawing_TileMode;

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object with a single color.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param color Color of the shader.
 * @return Returns the pointer to the {@link OH_Drawing_ShaderEffect} object created.
 *         If NULL is returned, the creation fails. The possible failure cause is that no memory is available.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateColorShader(const uint32_t color);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a linear gradient between two points.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If any of <b>startPt</b>, <b>endPt</b>, and <b>colors</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_TileMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param startPt Start point.
 * @param endPt End point.
 * @param colors Colors to distribute between the two points.
 * @param pos Relative position of each color in the color array. The array length must be the same as that of
 * <b>colors</b>. If <b>pos</b> is NULL, colors are evenly distributed between the start point and end point.
 * @param size Number of colors and positions (if <b>pos</b> is not NULL).
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateLinearGradient(const OH_Drawing_Point* startPt,
    const OH_Drawing_Point* endPt, const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a linear gradient between two points.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If any of <b>startPt</b>, <b>endPt</b>, and <b>colors</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_TileMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param startPt Start point.
 * @param endPt End point.
 * @param colors Colors to distribute between the two points.
 * @param pos Relative position of each color in the color array. The array length must be the same as that of
 * <b>colors</b>. If <b>pos</b> is NULL, colors are evenly distributed between the start point and end point.
 * @param size Number of colors and positions (if <b>pos</b> is not NULL).
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @param OH_Drawing_Matrix Matrix applied on the shader effect.
 * If <b>matrix</b> is NULL, an identity matrix is applied by default.
 * @return Returns the pointer to the {@link OH_Drawing_ShaderEffect} object created.
 * If NULL is returned, the creation fails.
 * The possible failure cause is that no memory is available or at least one of the parameters <b>startPt</b>,
 * <b>endPt</b>, and <b>colors</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateLinearGradientWithLocalMatrix(
    const OH_Drawing_Point2D* startPt, const OH_Drawing_Point2D* endPt, const uint32_t* colors, const float* pos,
    uint32_t size, OH_Drawing_TileMode, const OH_Drawing_Matrix*);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a radial gradient based on the center and
 * radius of a circle. The radial gradient transitions colors from the center to the ending shape in a radial manner.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>centerPt</b> or <b>colors</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b>} is returned.
 * If <b>OH_Drawing_TileMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt Center of the circle.
 * @param radius Radius of the gradient.
 * @param colors Colors to distribute in the radial direction.
 * @param pos Relative position of each color in the color array. The array length must be the same as that of
 * <b>colors</b>. If <b>pos</b> is NULL, colors are evenly distributed in the radial direction.
 * @param size Number of colors and positions (if <b>pos</b> is not NULL).
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateRadialGradient(const OH_Drawing_Point* centerPt, float radius,
    const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a radial gradient based on the center and
 * radius of a circle. The radial gradient transitions colors from the center to the ending shape in a radial manner.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>centerPt</b> or <b>colors</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b>} is returned.
 * If <b>OH_Drawing_TileMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt Center of the circle.
 * @param radius Radius of the gradient.
 * @param colors Colors to distribute in the radial direction.
 * @param pos Relative position of each color in the color array. The array length must be the same as that of
 * <b>colors</b>. If <b>pos</b> is NULL, colors are evenly distributed in the radial direction.
 * @param size Number of colors and positions (if <b>pos</b> is not NULL).
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @param OH_Drawing_Matrix Matrix applied on the shader effect.
 * If <b>matrix</b> is NULL, an identity matrix is applied by default.
 * @return Returns the pointer to the {@link OH_Drawing_ShaderEffect} object created.
 * If NULL is returned, the creation fails.
 * The possible failure cause is that no memory is available or at least one of the parameters
 * <b>centerPt</b> and <b>colors</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateRadialGradientWithLocalMatrix(
    const OH_Drawing_Point2D* centerPt, float radius, const uint32_t* colors, const float* pos, uint32_t size,
    OH_Drawing_TileMode, const OH_Drawing_Matrix*);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a sweep gradient based on the center.
 * A sweep gradient paints a gradient in a sweeping arc ranging from 0° to 360°.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>centerPt</b> or <b>colors</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b>} is returned.
 * If <b>OH_Drawing_TileMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param centerPt Center of the circle.
 * @param colors Colors to distribute between the two points.
 * @param pos Relative position of each color in the color array. The array length must be the same as that of
 * <b>colors</b>. If <b>pos</b> is NULL, colors are evenly distributed between the start angle (0°) and
 * end angle (360°).
 * @param size Number of colors and positions (if <b>pos</b> is not NULL).
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 *        For details about the available options, see {@link OH_Drawing_TileMode}.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateSweepGradient(const OH_Drawing_Point* centerPt,
    const uint32_t* colors, const float* pos, uint32_t size, OH_Drawing_TileMode);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object for an image shader.
 * You are advised not to use the function for the canvas of the capture type because it affects the performance.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Image</b> or <b>OH_Drawing_SamplingOptions</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If either <b>tileX</b> or <b>tileY</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Image Pointer to an {@link OH_Drawing_Image} object.
 * @param tileX Tile mode of the shader effect in the horizontal direction.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @param tileY Tile mode of the shader effect in the vertical direction.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @param OH_Drawing_SamplingOptions Pointer to an {@link OH_Drawing_SamplingOptions} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * If the pointer array is empty, the identity matrix is passed in.
 * @return Returns the pointer to the <b>OH_Drawing_ShaderEffect</b> object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateImageShader(OH_Drawing_Image*,
    OH_Drawing_TileMode tileX, OH_Drawing_TileMode tileY, const OH_Drawing_SamplingOptions*, const OH_Drawing_Matrix*);

/**
 * @brief Creates an <b>OH_Drawing_ShaderEffect</b> object that generates a gradient between two given circles.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If any of <b>startPt</b>, <b>endPt</b>, and <b>colors</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_TileMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param startPt Pointer to the center of the start circle.
 * @param startRadius Radius of the start circle.
 * @param endPt Pointer to the center of the end circle.
 * @param endRadius Radius of the end circle.
 * @param colors Colors to distribute between the two circles.
 * @param pos Relative position of each color in the color array. The array length must be the same as that of
 * <b>colors</b>. If <b>pos</b> is NULL, colors are evenly distributed between the two circles.
 * @param size Number of colors and positions (if <b>pos</b> is not NULL).
 * @param OH_Drawing_TileMode Tile mode of the shader effect.
 * For details about the available options, see {@link OH_Drawing_TileMode}.
 * @param OH_Drawing_Matrix Matrix applied on the shader effect.
 * If <b>matrix</b> is NULL, an identity matrix is applied by default.
 * @return Returns the pointer to the {@link OH_Drawing_ShaderEffect} object created.
 * If NULL is returned, the creation fails.
 * The possible failure cause is that no memory is available or at least one of the parameters <b>startPt</b>,
 * <b>endPt</b>, and <b>colors</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ShaderEffect* OH_Drawing_ShaderEffectCreateTwoPointConicalGradient(const OH_Drawing_Point2D* startPt,
    float startRadius, const OH_Drawing_Point2D* endPt, float endRadius, const uint32_t* colors, const float* pos,
    uint32_t size, OH_Drawing_TileMode, const OH_Drawing_Matrix*);

/**
 * @brief Destroys an <b>OH_Drawing_ShaderEffect</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ShaderEffect Pointer to an <b>OH_Drawing_ShaderEffect</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_ShaderEffectDestroy(OH_Drawing_ShaderEffect*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
