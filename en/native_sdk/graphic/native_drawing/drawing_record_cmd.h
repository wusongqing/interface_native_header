/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_RECORD_CMD_H
#define C_INCLUDE_DRAWING_RECORD_CMD_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 13
 * @version 1.0
 */

/**
 * @file drawing_record_cmd.h
 *
 * @brief Declares the functions related to a recording command object.
 *
 * File to include: native_drawing/drawing_record_cmd.h
 * @library libnative_drawing.so
 * @since 13
 * @version 1.0
 */

#include "drawing_types.h"
#include "drawing_error_code.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_RecordCmdUtils</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_RecordCmdUtils</b> object created.
 * @since 13
 * @version 1.0
 */
OH_Drawing_RecordCmdUtils* OH_Drawing_RecordCmdUtilsCreate(void);

/**
 * @brief Destroys an <b>OH_Drawing_RecordCmdUtils</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param recordCmdUtils Pointer to an {@link OH_Drawing_RecordCmdUtils} object.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if <b>recordCmdUtils</b> is NULL.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_RecordCmdUtilsDestroy(OH_Drawing_RecordCmdUtils* recordCmdUtils);

/**
 * @brief Starts recording. This function must be used in pair with {@link OH_Drawing_RecordCmdUtilsFinishRecording}. \n
 * The <b>OH_Drawing_RecordCmdUtils</b> object generates a canvas object of the recording type and
 * calls the interface of the drawing object to record all drawing commands.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param recordCmdUtils Pointer to an {@link OH_Drawing_RecordCmdUtils} object.
 * @param width Width of the canvas.
 * @param height Height of the canvas.
 * @param canvas Double pointer to the {@link OH_Drawing_Canvas} object. You do not need to release this pointer.
 * This object does not support nested calling of {@link OH_Drawing_CanvasDrawRecordCmd}.
 * @return Returns one of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>recordCmdUtils</b> or <b>canvas</b> is NULL.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>width</b> or <b>height</b> is less than 0.
 *         <b>OH_DRAWING_ERROR_ALLOCATION_FAILED</b> if the system memory is insufficient.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_RecordCmdUtilsBeginRecording(OH_Drawing_RecordCmdUtils* recordCmdUtils,
    int32_t width, int32_t height, OH_Drawing_Canvas** canvas);

/**
 * @brief Ends recording. This function must be called after {@link OH_Drawing_RecordCmdUtilsBeginRecording}. \n
 * The <b>OH_Drawing_RecordCmdUtils</b> object ends recording and stores the drawing commands recorded
 * by the canvas object of the recording type into the generated {@link OH_Drawing_RecordCmd} object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param recordCmdUtils Pointer to an {@link OH_Drawing_RecordCmdUtils} object.
 * @param recordCmd Double pointer to the {@link OH_Drawing_RecordCmd} object.
 * You need to call {@link OH_Drawing_CanvasDrawRecordCmd} to draw the object,
 * and call {@link OH_Drawing_RecordCmdDestroy} to release it.
 * @return Returns one of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>recordCmdUtils</b> or <b>recordCmd</b> is NULL.
 *         <b>OH_DRAWING_ERROR_ALLOCATION_FAILED</b> if the system memory is insufficient.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_RecordCmdUtilsFinishRecording(OH_Drawing_RecordCmdUtils* recordCmdUtils,
    OH_Drawing_RecordCmd** recordCmd);

/**
 * @brief Destroys an <b>OH_Drawing_RecordCmd</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param recordCmd Pointer to an {@link OH_Drawing_RecordCmd} object.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if <b>recordCmd</b> is NULL.
 * @since 13
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_RecordCmdDestroy(OH_Drawing_RecordCmd* recordCmd);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
