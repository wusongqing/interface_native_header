/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_DECLARATION_H
#define C_INCLUDE_DRAWING_TEXT_DECLARATION_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_declaration.h
 *
 * @brief Declares the structs related to text in 2D drawing.
 *
 * File to include: native_drawing/drawing_text_declaration.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Defines a struct used to load fonts.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_FontCollection OH_Drawing_FontCollection;

/**
 * @brief Defines a struct used to manage the typography layout and display.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_Typography OH_Drawing_Typography;

/**
 * @brief Defines a struct used to manage text colors and decorations.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_TextStyle OH_Drawing_TextStyle;

/**
 * @brief Defines a struct used to manage the typography style, such as the text direction.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_TypographyStyle OH_Drawing_TypographyStyle;

/**
 * @brief Defines a struct used to extract a single line of data from a piece of text for typography.
 *
 * @since 14
 * @version 1.0
 */
typedef struct OH_Drawing_LineTypography OH_Drawing_LineTypography;

/**
 * @brief Creates an {@link OH_Drawing_Typography}.
 *
 * @since 8
 * @version 1.0
 */
typedef struct OH_Drawing_TypographyCreate OH_Drawing_TypographyCreate;

/**
 * @brief Defines a struct for a text box, which is used to receive the rectangle size, direction, and quantity.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_TextBox OH_Drawing_TextBox;

/**
 * @brief Defines a struct used to receive the position and affinity of the graph.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_PositionAndAffinity OH_Drawing_PositionAndAffinity;

/**
 * @brief Defines a struct for a range, which is used to receive the start position and end position of the font.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_Range OH_Drawing_Range;

/**
 * @brief Defines a struct used to manage text shadows.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_TextShadow OH_Drawing_TextShadow;

/**
 * @brief Defines a struct used to parse system font files.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontParser OH_Drawing_FontParser;

/**
 * @brief Defines a struct used to manage text tabs.
 *
 * @since 14
 * @version 1.0
 */
typedef struct OH_Drawing_TextTab OH_Drawing_TextTab;

#ifdef __cplusplus
}
#endif
/** @} */
#endif
