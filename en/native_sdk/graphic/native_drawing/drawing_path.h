/*
 * Copyright (c) 2021-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_PATH_H
#define C_INCLUDE_DRAWING_PATH_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_path.h
 *
 * @brief Declares the functions related to the path in the drawing module.
 *
 File to include: native_drawing/drawing_path.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the directions of a closed contour.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathDirection {
    /** Adds a closed contour clockwise. */
    PATH_DIRECTION_CW,
    /** Adds a closed contour counterclockwise. */
    PATH_DIRECTION_CCW,
} OH_Drawing_PathDirection;

/**
 * @brief Enumerates the fill types of a path.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathFillType {
    /** Specifies that "inside" is computed by a non-zero sum of signed edge crossings.
     *  Specifically, draws a point and emits a ray in any direction. A count is used to record the number of
     *  intersection points of the ray and path, and the initial count is 0.
     *  When encountering a clockwise intersection point (the path passes from the left to the right of the ray),
     *  the count increases by 1. When encountering a counterclockwise intersection point (the path passes from the
     *  right to the left of the ray), the count decreases by 1. If the final count is not 0, the point is inside
     *  the path and needs to be colored. If the final count is 0, the point is not colored. */
    PATH_FILL_TYPE_WINDING,
    /** Specifies that "inside" is computed by an odd number of edge crossings.
     *  Specifically, draws a point and emits a ray in any direction. If the number of intersection points of the ray
     *  and path is an odd number, the point is considered to be inside the path and needs to be colored.
     *  If the number is an even number, the point is not colored. */
    PATH_FILL_TYPE_EVEN_ODD,
    /** Same as <b>PATH_FILL_TYPE_WINDING</b>, but draws outside of the path, rather than inside. */
    PATH_FILL_TYPE_INVERSE_WINDING,
    /** Same as <b>PATH_FILL_TYPE_EVEN_ODD</b>, but draws outside of the path, rather than inside. */
    PATH_FILL_TYPE_INVERSE_EVEN_ODD,
} OH_Drawing_PathFillType;

/**
 * @brief Enumerates the path adding modes.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathAddMode {
    /** Adds a path in append mode. */
    PATH_ADD_MODE_APPEND,
    /** Adds a line segment to close the path if the previous path is not closed. */
    PATH_ADD_MODE_EXTEND,
} OH_Drawing_PathAddMode;

/**
 * @brief Enumerates the operation modes available for a path.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathOpMode {
    /**
     * Difference operation.
     */
    PATH_OP_MODE_DIFFERENCE,
    /**
     * Intersection operation.
     */
    PATH_OP_MODE_INTERSECT,
    /**
     * Union operation.
     */
    PATH_OP_MODE_UNION,
    /**
     * XOR operation.
     */
    PATH_OP_MODE_XOR,
    /**
     * Reverse difference operation.
     */
    PATH_OP_MODE_REVERSE_DIFFERENCE,
} OH_Drawing_PathOpMode;

/**
 * @brief Enumerates the types of matrix information obtained during path measurement.
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_PathMeasureMatrixFlags {
    /**
     * Obtains the matrix corresponding to the location information.
     */
    GET_POSITION_MATRIX,
    /**
     * Obtains the matrix corresponding to the tangent information.
     */
    GET_TANGENT_MATRIX,
    /**
     * Obtains the matrix corresponding to the location and tangent information.
     */
    GET_POSITION_AND_TANGENT_MATRIX,
} OH_Drawing_PathMeasureMatrixFlags;

/**
 * @brief Creates an <b>OH_Drawing_Path</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_Path</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCreate(void);

/**
 * @brief Copies an existing {@link OH_Drawing_Path} object to create a new one.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @return Returns the pointer to the {@link OH_Drawing_Path} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Path* OH_Drawing_PathCopy(OH_Drawing_Path*);

/**
 * @brief Destroys an <b>OH_Drawing_Path</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathDestroy(OH_Drawing_Path*);

/**
 * @brief Sets the start point of a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param x X coordinate of the start point.
 * @param y Y coordinate of the start point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief Draws a line segment from the last point of a path to the target point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param x X coordinate of the target point.
 * @param y Y coordinate of the target point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief Draws an arc to a path. This is done by using angle arc mode. In this mode, a rectangle is specified first,
 * and then a start angle and scanning degree are specified. The inscribed ellipse of the rectangle will be used to
 * intercept the arc. The arc is a portion of the ellipse defined by the start angle and the sweep angle.
 * If the path is empty, a line segment from the last point of the path to the start point of the arc is also added.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param x1 X coordinate of the upper left corner of the rectangle.
 * @param y1 Y coordinate of the upper left corner of the rectangle.
 * @param x2 X coordinate of the lower right corner of the rectangle.
 * @param y2 Y coordinate of the lower right corner of the rectangle.
 * @param startDeg Start angle. The start direction (0°) of the angle is the positive direction of the X axis.
 * @param sweepDeg Sweep degree. A positive value indicates a clockwise swipe, and a negative value indicates a
 * counterclockwise swipe. The actual swipe degree is the modulo operation result of the input parameter by 360.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathArcTo(OH_Drawing_Path*, float x1, float y1, float x2, float y2, float startDeg, float sweepDeg);

/**
 * @brief Draws a quadratic Bezier curve from the last point of a path to the target point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param ctrlX X coordinate of the control point.
 * @param ctrlY Y coordinate of the control point.
 * @param endX X coordinate of the target point.
 * @param endY Y coordinate of the target point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief Draws a conic curve from the last point of a path to the target point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param ctrlX X coordinate of the control point.
 * @param ctrlY Y coordinate of the control point.
 * @param endX X coordinate of the target point.
 * @param endY Y coordinate of the target point.
 * @param weight Weight of the curve, which determines its shape. The larger the value, the closer of the curve to the
 * control point. If the value is less than or equal to 0, this function is equivalent to {@link OH_Drawing_PathLineTo},
 * that is, adding a line segment from the last point of the path to the target point.
 * If the value is 1, this function is equivalent to {@link OH_Drawing_PathQuadTo}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathConicTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY, float weight);

/**
 * @brief Draws a cubic Bezier curve from the last point of a path to the target point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @param ctrlX1 X coordinate of the first control point.
 * @param ctrlY1 Y coordinate of the first control point.
 * @param ctrlX2 X coordinate of the second control point.
 * @param ctrlY2 Y coordinate of the second control point.
 * @param endX X coordinate of the target point.
 * @param endY Y coordinate of the target point.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief Sets the start position relative to the last point of a path.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param x X offset relative to the last point, which is used to specify the X coordinate of the start point.
 * @param y Y offset relative to the last point, which is used to specify the Y coordinate of the start point.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRMoveTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief Draws a line segment from the last point of a path to a point relative to the last point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param x X offset relative to the last point, which is used to specify the X coordinate of the target point.
 * @param y Y offset relative to the last point, which is used to specify the X coordinate of the target point.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRLineTo(OH_Drawing_Path*, float x, float y);

/**
 * @brief Draws a quadratic Bezier curve from the last point of a path to a point relative to the last point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param ctrlX X offset relative to the last point, which is used to specify the X coordinate of the control point.
 * @param ctrlY Y offset relative to the last point, which is used to specify the Y coordinate of the control point.
 * @param endX X offset relative to the last point, which is used to specify the X coordinate of the target point.
 * @param endY Y offset relative to the last point, which is used to specify the Y coordinate of the target point.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRQuadTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY);

/**
 * @brief Draws a conic curve from the last point of a path to a point relative to the last point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param ctrlX X offset relative to the last point, which is used to specify the X coordinate of the control point.
 * @param ctrlY Y offset relative to the last point, which is used to specify the Y coordinate of the control point.
 * @param endX X offset relative to the last point, which is used to specify the X coordinate of the target point.
 * @param endY Y offset relative to the last point, which is used to specify the Y coordinate of the target point.
 * @param weight Weight of the curve, which determines its shape. The larger the value, the closer of the curve to
 * the control point. If the value is less than or equal to 0, this function is equivalent to
 * {@link OH_Drawing_PathRLineTo}, that is, adding a line segment from the last point of the path to the target point.
 * If the value is 1, this function is equivalent to {@link OH_Drawing_PathRQuadTo}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRConicTo(OH_Drawing_Path*, float ctrlX, float ctrlY, float endX, float endY, float weight);

/**
 * @brief Draws a cubic Bezier curve from the last point of a path to a point relative to the last point.
 * If the path is empty, the start point (0, 0) is used.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param ctrlX1 X offset relative to the last point, which is used to specify the X coordinate of
 * the first control point.
 * @param ctrlY1 Y offset relative to the last point, which is used to specify the Y coordinate of
 * the first control point.
 * @param ctrlX2 X offset relative to the last point, which is used to specify the X coordinate of
 * the second control point.
 * @param ctrlY2 Y offset relative to the last point, which is used to specify the Y coordinate of
 * the second control point.
 * @param endX X offset relative to the last point, which is used to specify the X coordinate of the target point.
 * @param endY Y offset relative to the last point, which is used to specify the Y coordinate of the target point.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathRCubicTo(
    OH_Drawing_Path*, float ctrlX1, float ctrlY1, float ctrlX2, float ctrlY2, float endX, float endY);

/**
 * @brief Adds a rectangle contour to a path in the specified direction.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathDirection</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param left X coordinate of the upper left corner of the rectangle.
 * @param top Y coordinate of the upper left corner of the rectangle.
 * @param right X coordinate of the lower right corner of the rectangle.
 * @param bottom Y coordinate of the lower right corner of the rectangle.
 * @param OH_Drawing_PathDirection Path direction.
  For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRect(OH_Drawing_Path*, float left, float top,
    float right, float bottom, OH_Drawing_PathDirection);

/**
 * @brief Adds a rectangle contour to a path in the specified direction.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathDirection</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @param start Start point, indicating the corner of the rectangle from which the path is drawn.
 * The value <b>0</b> means the upper left corner, <b>1</b> means the upper right corner,
 * <b>2</b> means the lower right corner, and <b>3</b> means the lower left corner.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRectWithInitialCorner(OH_Drawing_Path*, const OH_Drawing_Rect*,
    OH_Drawing_PathDirection, uint32_t start);

/**
 * @brief Adds a rounded rectangle contour to a path in the specified direction.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>roundRect</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathDirection</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param roundRect Pointer to an {@link OH_Drawing_RoundRect} object.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddRoundRect(OH_Drawing_Path*, const OH_Drawing_RoundRect* roundRect, OH_Drawing_PathDirection);

/**
 * @brief Adds an oval to a path. <b>OH_Drawing_Rect</b> specifies the outer tangent rectangle of the oval,
 * and <b>OH_Drawing_PathDirection</b> specifies whether the drawing is clockwise or anticlockwise.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathDirection</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param start Start point of the oval.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddOvalWithInitialPoint(OH_Drawing_Path*, const OH_Drawing_Rect*,
    uint32_t start, OH_Drawing_PathDirection);

/**
 * @brief Adds an arc to a path as the start of a new contour. The arc added is part of the inscribed ellipse
 * of the rectangle, from the start angle through the sweep angle. If the sweep angle is less than or equal to -360°,
 * or if the sweep angle is greater than or equal to 360°, and start angle modulo 90 is nearly zero,
 * an oval instead of an ellipse is added.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param startAngle Start angle of the arc, in degrees.
 * @param sweepAngle Angle to sweep, in degrees. A positive number indicates a clockwise sweep,
 * and a negative number indicates a counterclockwise sweep.
 * The actual swipe degree is the modulo operation result of the input parameter by 360.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddArc(OH_Drawing_Path*, const OH_Drawing_Rect*, float startAngle, float sweepAngle);

/**
 * @brief Transforms the points in a <b>src</b> path by a matrix and adds the new one to the current path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>src</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param src Pointer to a source path, which is an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * If NULL is passed in, it is the identity matrix.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPath(OH_Drawing_Path*, const OH_Drawing_Path* src, const OH_Drawing_Matrix*);

/**
 * @brief Transforms the points in a <b>src</b> path by a matrix and adds the new one to the current path
 * with the specified adding mode.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>src</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathAddMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param src Pointer to a source path, which is an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * If NULL is passed in, it is the identity matrix.
 * @param OH_Drawing_PathAddMode Path adding mode.
 * For details about the available options, see {@link OH_Drawing_PathAddMode}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPathWithMatrixAndMode(OH_Drawing_Path* path, const OH_Drawing_Path* src,
    const OH_Drawing_Matrix*, OH_Drawing_PathAddMode);

/**
 * @brief Adds a <b>src</b> path to the current path with the specified adding mode.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>src</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathAddMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param src Pointer to a source path, which is an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_PathAddMode Path adding mode.
 * For details about the available options, see {@link OH_Drawing_PathAddMode}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPathWithMode(OH_Drawing_Path* path, const OH_Drawing_Path* src, OH_Drawing_PathAddMode);

/**
 * @brief Translates a <b>src</b> path by an offset and adds the new one to the current path
 * with the specified adding mode.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>src</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathAddMode</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param src Pointer to a source path, which is an {@link OH_Drawing_Path} object.
 * @param dx X offset.
 * @param dy Y offset.
 * @param OH_Drawing_PathAddMode Path adding mode.
 * For details about the available options, see {@link OH_Drawing_PathAddMode}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPathWithOffsetAndMode(OH_Drawing_Path* path, const OH_Drawing_Path* src,
    float dx, float dy, OH_Drawing_PathAddMode);

/**
 * @brief Adds a polygon to a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>points</b> is NULL or <b>count</b> is <b>0</b>,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param points Pointer to an array that holds the vertex coordinates of the polygon.
 * @param count Size of the array.
 * @param isClosed Whether the path is closed.
 * The value <b>true</b> means that the path is closed, and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddPolygon(OH_Drawing_Path* path, const OH_Drawing_Point2D* points, uint32_t count, bool isClosed);

/**
 * @brief Adds a circle to a path in the specified direction.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>radius</b> is less than or equal to 0, <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 * If <b>OH_Drawing_PathDirection</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to an {@link OH_Drawing_Path} object.
 * @param x X coordinate of the circle center.
 * @param y Y coordinate of the circle center.
 * @param radius Radius of the circle.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddCircle(OH_Drawing_Path* path, float x, float y, float radius, OH_Drawing_PathDirection);

/**
 * @brief Adds an oval to a path in the specified direction.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathDirection</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @param OH_Drawing_PathDirection Path direction.
 * For details about the available options, see {@link OH_Drawing_PathDirection}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathAddOval(OH_Drawing_Path*, const OH_Drawing_Rect*, OH_Drawing_PathDirection);

/**
 * @brief Parses the path represented by an SVG string.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>str</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to an {@link OH_Drawing_Path} object.
 * @param str Pointer to the SVG string.
 * @return Returns <b>true</b> if the SVG string is parsed successfully; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathBuildFromSvgString(OH_Drawing_Path* path, const char* str);

/**
 * @brief Checks whether a coordinate point is included in a path. For details, see {@link OH_Drawing_PathFillType}.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param x Coordinate point on the X axis.
 * @param y Coordinate point on the Y axis.
 * @return Returns <b>true</b> if the coordinate point is included in the path; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathContains(OH_Drawing_Path*, float x, float y);

/**
 * @brief Transforms the points in a path by a matrix.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>OH_Drawing_Matrix</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathTransform(OH_Drawing_Path*, const OH_Drawing_Matrix*);

/**
 * @brief Transforms the points in a <b>src</b> path by a matrix and uses the new one to replace the <b>dst</b> path.
 * If <b>dst</b> is NULL, the <b>src</b> path is replaced.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>src</b> or <b>OH_Drawing_Matrix</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param src Pointer to a source path, which is an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Matrix Pointer to an {@link OH_Drawing_Matrix} object.
 * @param dst Pointer to a destination path, which is an {@link OH_Drawing_Path} object.
 * @param applyPerspectiveClip Whether to apply perspective cropping to the new path.
 * The value <b>true</b> means to apply perspective cropping, and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathTransformWithPerspectiveClip(OH_Drawing_Path* src, const OH_Drawing_Matrix*,
    OH_Drawing_Path* dst, bool applyPerspectiveClip);

/**
 * @brief Sets the fill type for a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>OH_Drawing_PathFillType</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_PathFillType Fill type of the path.
 * For details about the available options, see {@link OH_Drawing_PathFillType}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathSetFillType(OH_Drawing_Path*, OH_Drawing_PathFillType);

/**
 * @brief Obtains the length of a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param forceClosed Whether the path can be modified or deleted freely after the function is called.
 * The value <b>true</b> means that the path can be modified or deleted freely, and <b>false</b> means the opposite.
 * @return Returns the length of the path.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_PathGetLength(OH_Drawing_Path*, bool forceClosed);

/**
 * @brief Obtains the minimum bounds that enclose a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>OH_Drawing_Path</b> or <b>OH_Drawing_Rect</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @param OH_Drawing_Rect Pointer to an {@link OH_Drawing_Rect} object.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathGetBounds(OH_Drawing_Path*, OH_Drawing_Rect*);

/**
 * @brief Closes a path by drawing a line segment from the current point to the start point of the path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an {@link OH_Drawing_Path} object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathClose(OH_Drawing_Path*);

/**
 * @brief Checks whether a path is closed.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>path</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to an {@link OH_Drawing_Path} object.
 * @param forceClosed Whether the path is measured as a closed path. The value <b>true</b> means that the path is
 * considered closed during measurement, and <b>false</b> means that the path is measured
 * based on the actual closed status.
 * @return Returns <b>true</b> if the path is closed; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathIsClosed(OH_Drawing_Path* path, bool forceClosed);

/**
 * @brief Obtains the coordinates and tangent at a distance from the start point of a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If any of <b>path</b>, <b>position</b>, or <b>tangent</b> is NULL,
 * <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to an {@link OH_Drawing_Path} object.
 * @param forceClosed Whether the path is measured as a closed path. The value <b>true</b> means that the path is
 * considered closed during measurement, and <b>false</b> means that the path is measured
 * based on the actual closed status.
 * @param distance Distance from the start point. If the distance is less than 0, it is considered as 0.
 * If the distance is greater than the path length, it is considered as the path length.
 * @param position Pointer to the coordinates.
 * @param tangent Pointer to the tangent.
 * @return Returns <b>true</b> if the operation is successful; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathGetPositionTangent(OH_Drawing_Path* path, bool forceClosed,
    float distance, OH_Drawing_Point2D* position, OH_Drawing_Point2D* tangent);

/**
 * @brief Combines two paths based on the specified operation mode.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>srcPath</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>op</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to an {@link OH_Drawing_Path} object, in which the resulting path is saved.
 * @param other Pointer to an {@link OH_Drawing_Path} object.
 * @param op Operation mode of the path. For details about the available options, see {@link OH_Drawing_PathOpMode}.
 * @return Returns <b>true</b> if the resulting path is not empty; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathOp(OH_Drawing_Path* path, const OH_Drawing_Path* other, OH_Drawing_PathOpMode op);

/**
 * @brief Obtains a transformation matrix at a distance from the start point of a path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If either <b>path</b> or <b>matrix</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 * If <b>flag</b> is not set to one of the enumerated values,
 * <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to an {@link OH_Drawing_Path} object.
 * @param forceClosed Whether the path is measured as a closed path. The value <b>true</b> means that the path is
 * considered closed during measurement, and <b>false</b> means that the path is measured
 * based on the actual closed status.
 * @param distance Distance from the start point. If the distance is less than 0, it is considered as 0.
 * If the distance is greater than the path length, it is considered as the path length.
 * @param matrix Pointer to the transformation matrix.
 * @param flag Type of the matrix information.
 * For details about the available options, see {@link OH_Drawing_PathMeasureMatrixFlags}.
 * @return Returns <b>true</b> if the transformation matrix is obtained successfully; returns <b>false</b> otherwise.
 * The possible failure cause is that <b>path</b> is NULL or the path length is 0.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_PathGetMatrix(OH_Drawing_Path* path, bool forceClosed,
    float distance, OH_Drawing_Matrix* matrix, OH_Drawing_PathMeasureMatrixFlags flag);

/**
 * @brief Translates a path by an offset along the X axis and Y axis and adds the new one to the <b>dst</b> path.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>path</b> is NULL, {@link OH_DRAWING_ERROR_INVALID_PARAMETER} is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param path Pointer to the current path, which is an {@link OH_Drawing_Path} object.
 * @param dst Pointer to a destination path, which is an {@link OH_Drawing_Path} object.
 * If NULL is passed in, the result is stored in the current path.
 * @param dx X offset.
 * @param dy Y offset.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_PathOffset(OH_Drawing_Path* path, OH_Drawing_Path* dst, float dx, float dy);

/**
 * @brief Resets path data.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>OH_Drawing_Path</b> is NULL, <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Path Pointer to an <b>OH_Drawing_Path</b> object.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_PathReset(OH_Drawing_Path*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
