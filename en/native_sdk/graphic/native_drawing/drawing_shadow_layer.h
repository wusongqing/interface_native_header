/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_SHADOW_LAYER_H
#define C_INCLUDE_DRAWING_SHADOW_LAYER_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_shadow_layer.h
 *
 * @brief Declares the functions related to the shadow in the drawing module.
 *
 * File to include: native_drawing/drawing_shadow_layer.h
 * @library libnative_drawing.so
 * @since 12
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_ShadowLayer</b> object.
 *
 * Error codes may be generated in the call. You can view the error code by calling {@link OH_Drawing_ErrorCodeGet}.
 * If <b>blurRadius</b> is less than or equal to <b>0</b>, <b>OH_DRAWING_ERROR_PARAMETER_OUT_OF_RANGE</b> is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param blurRadius Radius of the shadow layer. The value must be greater than 0.
 * @param x Offset on the X axis.
 * @param y Offset on the Y axis.
  * @param color Color of the shadow.
 * @return Returns the pointer to the <b>OH_Drawing_ShadowLayer</b> object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ShadowLayer* OH_Drawing_ShadowLayerCreate(float blurRadius, float x, float y, uint32_t color);

/**
 * @brief Destroys an <b>OH_Drawing_ShadowLayer</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_ShadowLayer Pointer to the shadow layer.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_ShadowLayerDestroy(OH_Drawing_ShadowLayer*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
