/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file camera_device.h
 *
 * @brief 声明相机的基本概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 12
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_CAMERADEVICE_H
#define NATIVE_INCLUDE_CAMERA_CAMERADEVICE_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"

#ifdef __cplusplus
extern "C" {

#endif
/**
 * @brief 获取相机设备的传感器方向属性。
 *
 * @param camera {@link Camera_Device} 用来获取属性。
 * @param orientation 返回相机sensor角度属性。
 * @return {@link #CAMERA_OK} 如果方法调用成功，则返回传感器方向属性。
 *         {@link #INVALID_ARGUMENT} 如果参数丢失或者参数不正确。
 *         {@link #CAMERA_SERVICE_FATAL_ERROR} 如果相机服务出现致命错误。
 * @since 12
 */
Camera_ErrorCode OH_CameraDevice_GetCameraOrientation(Camera_Device* camera, uint32_t* orientation);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_CAMERADEVICE_H
/** @} */