/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供image的native接口的访问.
 *
 * @since 12
 */

/**
 * @file image_native.h
 *
 * @brief 声明图像的剪辑矩形、大小和组件数据的接口函数。
 *
 * @library libohimage.so
 * @syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_H
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_H

#include "image_common.h"
#include "native_buffer.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 为图像接口定义native层图像对象。
 *
 * @since 12
 */
struct OH_ImageNative;

/**
 * @brief 为图像接口定义native层图像对象的别名。
 *
 * @since 12
 */
typedef struct OH_ImageNative OH_ImageNative;

/**
 * @brief 获取native {@link OH_ImageNative} 对象的 {@link Image_Size} 信息。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @param size 表示作为获取结果的 {@link Image_Size} 对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 如果未知原因错误返回 IMAGE_UNKNOWN_ERROR；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetImageSize(OH_ImageNative *image, Image_Size *size);

/**
 * @brief 获取native {@link OH_ImageNative} 对象的组件列表信息。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @param types 表示作为获取结果的组件列表对象的指针。
 * @param typeSize 表示作为获取结果的组件列表中，元素个数的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetComponentTypes(OH_ImageNative *image,
    uint32_t **types, size_t *typeSize);

/**
 * @brief 获取native {@link OH_ImageNative} 对象中某个组件类型所对应的缓冲区。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @param componentType 表示组件的类型。
 * @param nativeBuffer 表示作为获取结果的 {@link OH_NativeBuffer} 缓冲区对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetByteBuffer(OH_ImageNative *image,
    uint32_t componentType, OH_NativeBuffer **nativeBuffer);

/**
 * @brief 获取native {@link OH_ImageNative} 对象中某个组件类型所对应的缓冲区的大小。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @param componentType 表示组件的类型。
 * @param size 表示作为获取结果的缓冲区大小的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetBufferSize(OH_ImageNative *image,
    uint32_t componentType, size_t *size);

/**
 * @brief 获取native {@link OH_ImageNative} 对象中某个组件类型所对应的像素行宽。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @param componentType 表示组件的类型。
 * @param rowStride 表示作为获取结果的像素行宽的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetRowStride(OH_ImageNative *image,
    uint32_t componentType, int32_t *rowStride);

/**
 * @brief 获取native {@link OH_ImageNative} 对象中某个组件类型所对应的像素大小。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @param componentType 表示组件的类型。
 * @param pixelStride 表示作为获取结果的像素大小的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetPixelStride(OH_ImageNative *image,
    uint32_t componentType, int32_t *pixelStride);
	
/**
 * @brief 获取native {@link OH_ImageNative} 对象中的时间戳信息
 *
 * @param 表示 {@link OH_ImageNative} native对象的指针。
 * @param 表示作为获取结果的时间戳信息的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_GetTimestamp(OH_ImageNative *image, int64_t *timestamp);

/**
 * @brief 释放native {@link OH_ImageNative} 对象。
 *
 * @param image 表示 {@link OH_ImageNative} native对象的指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS；
 * 如果参数错误返回 IMAGE_BAD_PARAMETER；
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_ImageNative_Release(OH_ImageNative *image);

#ifdef __cplusplus
};
#endif
/** @} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_H
