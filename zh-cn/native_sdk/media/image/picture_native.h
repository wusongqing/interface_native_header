/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供获取picture数据和信息的API。
 *
 * @Syscap SystemCapability.Multimedia.Image.Core
 * @since 13
 */

/**
 * @file picture_native.h
 *
 * @brief 提供获取picture数据和信息的API。
 *
 * @library libpicture.so
 * @kit ImageKit
 * @Syscap SystemCapability.Multimedia.Image.Core
 * @since 13
 */
#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PICTURE_NATIVE_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PICTURE_NATIVE_H_
#include "image_common.h"
#include "pixelmap_native.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Picture结构体类型，用于执行picture相关操作。
 * Picture为多图对象结构体，包含主图、辅助图和元数据。
 * 主图包含图像的大部分信息，主要用于显示图像内容。
 * 辅助图用于存储与主图相关但不同的数据，展示图像更丰富的信息。
 * 元数据一般用来存储关于图像文件的信息。
 * @since 13
 */
struct OH_PictureNative;

/**
 * @brief Picture结构体类型，用于执行picture相关操作。
 *
 * @since 13
 */
typedef struct OH_PictureNative OH_PictureNative;

/**
 * @brief AuxiliaryPicture结构体类型，用于执行AuxiliaryPicture相关操作。
 *
 * @since 13
 */
struct OH_AuxiliaryPictureNative;

/**
 * @brief AuxiliaryPicture结构体类型，用于执行AuxiliaryPicture相关操作。
 *
 * @since 13
 */
typedef struct OH_AuxiliaryPictureNative OH_AuxiliaryPictureNative;

/**
 * @brief AuxiliaryPictureInfo结构体类型，用于执行AuxiliaryPictureInfo相关操作。
 *
 * @since 13
 */
struct OH_AuxiliaryPictureInfo;

/**
 * @brief AuxiliaryPictureInfo结构体类型，用于执行AuxiliaryPictureInfo相关操作。
 *
 * @since 13
 */
typedef struct OH_AuxiliaryPictureInfo OH_AuxiliaryPictureInfo;

/**
 * @brief 辅助图类型
 *
 * @since 13
 */
typedef enum {
    /*
    * 增益图，代表了一种增强SDR图像以产生具有可变显示调整能力的HDR图像的机制。它是一组描述如何应用gainmap元数据的组合。
    */
    AUXILIARY_PICTURE_TYPE_GAINMAP = 1,
    /*
    * 深度图，储存图像的深度数据，通过捕捉每个像素与摄像机之间的距离，提供场景的三维结构信息，通常用于3D重建和场景理解。
    */
    AUXILIARY_PICTURE_TYPE_DEPTH_MAP = 2,
    /*
    * 人像未对焦的原图，提供了一种在人像拍摄中突出背景模糊效果的方式，能够帮助用户在后期处理中选择焦点区域，增加创作自由度。
    */
    AUXILIARY_PICTURE_TYPE_UNREFOCUS_MAP = 3,
    /*
    * 线性图，用于提供额外的数据视角或补充信息，通常用于视觉效果的增强，它可以包含场景中光照、颜色或其他视觉元素的线性表示。
    */
    AUXILIARY_PICTURE_TYPE_LINEAR_MAP = 4,
    /*
    * 水印裁剪图，表示在原图中被水印覆盖的区域，该图像用于修复或移除水印影响，恢复图像的完整性和可视性。
    */
    AUXILIARY_PICTURE_TYPE_FRAGMENT_MAP = 5,
} Image_AuxiliaryPictureType;

/**
 * @brief 创建OH_PictureNative指针。
 *
 * @param mainPixelmap 主图的OH_PixelmapNative指针。
 * @param picture 被创建的OH_PictureNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_CreatePicture(OH_PixelmapNative *mainPixelmap, OH_PictureNative **picture);

/**
 * @brief 获取主图的OH_PixelmapNative指针。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param mainPixelmap 获取的OH_PixelmapNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_GetMainPixelmap(OH_PictureNative *picture, OH_PixelmapNative **mainPixelmap);

/**
 * @brief 获取hdr图的OH_PixelmapNative指针。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param hdrPixelmap 获取的hdr图OH_PixelmapNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的操作，例如picture对象中不包含增益图返回 IMAGE_UNSUPPORTED_OPERATION，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_GetHdrComposedPixelmap(OH_PictureNative *picture, OH_PixelmapNative **hdrPixelmap);

/**
 * @brief 获取增益图的OH_PixelmapNative指针。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param gainmapPixelmap 获取的增益图OH_PixelmapNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_GetGainmapPixelmap(OH_PictureNative *picture, OH_PixelmapNative **gainmapPixelmap);

/**
 * @brief 设置辅助图。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param type 辅助图的类型。
 * @param auxiliaryPicture 设置的OH_AuxiliaryPictureNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_SetAuxiliaryPicture(OH_PictureNative *picture, Image_AuxiliaryPictureType type,
    OH_AuxiliaryPictureNative *auxiliaryPicture);

/**
 * @brief 根据类型获取辅助图。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param type 辅助图类型。
 * @param auxiliaryPicture 获取的OH_AuxiliaryPictureNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_GetAuxiliaryPicture(OH_PictureNative *picture, Image_AuxiliaryPictureType type,
    OH_AuxiliaryPictureNative **auxiliaryPicture);

/**
 * @brief 获取主图的元数据。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param metadataType 元数据类型。
 * @param metadata 主图的元数据。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的元数据类型返回 IMAGE_UNSUPPORTED_METADATA，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_GetMetadata(OH_PictureNative *picture, Image_MetadataType metadataType,
    OH_PictureMetadata **metadata);

/**
 * @brief 设置主图的元数据。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @param metadataType 元数据类型。
 * @param metadata 将设置的元数据。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的元数据类型返回 IMAGE_UNSUPPORTED_METADATA，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_SetMetadata(OH_PictureNative *picture, Image_MetadataType metadataType,
    OH_PictureMetadata *metadata);

/**
 * @brief 释放OH_PictureNative指针。
 *
 * @param picture 被操作的OH_PictureNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PictureNative_Release(OH_PictureNative *picture);

/**
 * @brief 创建OH_AuxiliaryPictureNative指针。
 *
 * @param data 图像数据。
 * @param dataLength 图像数据长度。
 * @param size 辅助图尺寸。
 * @param type 辅助图类型。
 * @param auxiliaryPicture 被创建的OH_AuxiliaryPictureNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_Create(uint8_t *data, size_t dataLength, Image_Size *size,
    Image_AuxiliaryPictureType type, OH_AuxiliaryPictureNative **auxiliaryPicture);

/**
 * @brief 读取缓冲区的图像像素数据，并将结果写入为辅助图中。
 *
 * @param auxiliaryPicture 被操作的OH_AuxiliaryPictureNative指针。
 * @param source 将被写入的图像像素数据。
 * @param bufferSize 图像像素数据长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果内存分配失败返回 IMAGE_ALLOC_FAILED，如果内存拷贝失败返回 IMAGE_COPY_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_WritePixels(OH_AuxiliaryPictureNative *auxiliaryPicture, uint8_t *source,
    size_t bufferSize);

/**
 * @brief 读取辅助图的像素数据，结果写入缓冲区。
 *
 * @param auxiliaryPicture 被操作的OH_AuxiliaryPictureNative指针。
 * @param destination 缓冲区，获取的辅助图像素数据写入到该内存区域内。
 * @param bufferSize 缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果内存分配失败返回 IMAGE_ALLOC_FAILED，如果内存拷贝失败返回 IMAGE_COPY_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_ReadPixels(OH_AuxiliaryPictureNative *auxiliaryPicture, uint8_t *destination,
    size_t *bufferSize);

/**
 * @brief 获取辅助图类型。
 *
 * @param auxiliaryPicture 被操作的OH_AuxiliaryPictureNative指针。
 * @param type 辅助图类型。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_GetType(OH_AuxiliaryPictureNative *auxiliaryPicture,
    Image_AuxiliaryPictureType *type);

/**
 * @brief 获取辅助图信息。
 *
 * @param auxiliaryPicture 被操作的OH_AuxiliaryPictureNative指针。
 * @param info 辅助图信息
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_GetInfo(OH_AuxiliaryPictureNative *auxiliaryPicture,
    OH_AuxiliaryPictureInfo **info);

/**
 * @brief 设置辅助图信息。
 *
 * @param auxiliaryPicture 将操作的OH_AuxiliaryPictureNative指针。
 * @param info 将要设置的辅助图信息。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_SetInfo(OH_AuxiliaryPictureNative *auxiliaryPicture,
    OH_AuxiliaryPictureInfo *info);

/**
 * @brief 获取辅助图的元数据。
 *
 * @param auxiliaryPicture 将操作的OH_AuxiliaryPictureNative指针。
 * @param metadataType 元数据类型。
 * @param metadata 获取的元数据。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的元数据类型，或者元数据类型与辅助图片类型不匹配返回 IMAGE_UNSUPPORTED_METADATA，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_GetMetadata(OH_AuxiliaryPictureNative *auxiliaryPicture,
    Image_MetadataType metadataType, OH_PictureMetadata **metadata);

/**
 * @brief 设置辅助图的元数据。
 *
 * @param auxiliaryPicture 将操作的OH_AuxiliaryPictureNative指针。
 * @param metadataType 元数据类型。
 * @param metadata 将要设置的元数据。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果是不支持的元数据类型，或者元数据类型与辅助图片类型不匹配返回 IMAGE_UNSUPPORTED_METADATA，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_SetMetadata(OH_AuxiliaryPictureNative *auxiliaryPicture,
    Image_MetadataType metadataType, OH_PictureMetadata *metadata);

/**
 * @brief 释放OH_AuxiliaryPictureNative指针。
 *
 * @param picture 将操作的OH_AuxiliaryPictureNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureNative_Release(OH_AuxiliaryPictureNative *picture);

/**
 * @brief 创建一个OH_AuxiliaryPictureInfo对象。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_Create(OH_AuxiliaryPictureInfo **info);

/**
 * @brief 获取辅助图的图片信息的辅助图类型。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param type 获取的辅助图类型。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_GetType(OH_AuxiliaryPictureInfo *info, Image_AuxiliaryPictureType *type);

/**
 * @brief 设置辅助图的图片信息的辅助图类型。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param type 将要设置的辅助图类型。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_SetType(OH_AuxiliaryPictureInfo *info, Image_AuxiliaryPictureType type);

/**
 * @brief 获取辅助图的图片尺寸。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param size 获取的图片尺寸。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_GetSize(OH_AuxiliaryPictureInfo *info, Image_Size *size);

/**
 * @brief 设置辅助图的图片尺寸。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param size 将要设置的图片尺寸。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_SetSize(OH_AuxiliaryPictureInfo *info, Image_Size *size);

/**
 * @brief 获取辅助图的图片信息的行跨距。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param rowStride 跨距，内存中每行像素所占的空间。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_GetRowStride(OH_AuxiliaryPictureInfo *info, uint32_t *rowStride);

/**
 * @brief 设置辅助图的图片信息的行跨距。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param rowStride 跨距，内存中每行像素所占的空间。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_SetRowStride(OH_AuxiliaryPictureInfo *info, uint32_t rowStride);

/**
 * @brief 获取辅助图的图片信息的像素格式。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param pixelFormat 获取的像素格式。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_GetPixelFormat(OH_AuxiliaryPictureInfo *info, PIXEL_FORMAT *pixelFormat);

/**
 * @brief 设置辅助图的图片信息的像素格式。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @param pixelFormat 将要设置的像素格式。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_SetPixelFormat(OH_AuxiliaryPictureInfo *info, PIXEL_FORMAT pixelFormat);

/**
 * @brief 释放OH_AuxiliaryPictureInfo指针。
 *
 * @param info 将操作的OH_AuxiliaryPictureInfo指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体释义参考{@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_AuxiliaryPictureInfo_Release(OH_AuxiliaryPictureInfo *info);

#ifdef __cplusplus
};
#endif
/** @} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PICTURE_NATIVE_H_