/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Drm
 * @{
 *
 * @brief 提供数字版权保护能力的API。
 * @kit Drm.
 * @since 11
 * @version 1.0
 */

/**
 * @file native_mediakeysystem.h
 * @brief 定义Drm MediaKeySystem API。提供以下功能：
 * 查询是否支持特定的drm，创建媒体密钥会话，获取和设置配置，
 * 获取统计信息，获取内容保护级别，生成提供请求，处理提供响应，
 * 事件监听，获取内容防护级别，管理离线媒体密钥等。
 * @library libnative_drm.z.so
 * @syscap SystemCapability.Multimedia.Drm.Core
 * @since 11
 * @version 1.0
 */

#ifndef OHOS_DRM_NATIVE_MEDIA_KEY_SYSTEM_H
#define OHOS_DRM_NATIVE_MEDIA_KEY_SYSTEM_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "native_drm_err.h"
#include "native_drm_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 事件触发时将调用的回调，不返回媒体密钥系统实例，适用于单个媒体密钥系统场景。
 * @param eventType 事件类型。
 * @param info 从媒体密钥系统获取的事件信息。
 * @param infoLen 事件信息长度。
 * @param extra 从媒体密钥系统获得的额外信息。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数无效。
 * @since 11
 * @version 1.0
 */
typedef  Drm_ErrCode (*MediaKeySystem_Callback)(DRM_EventType eventType, uint8_t *info,
    int32_t infoLen, char *extra);

/**
 * @brief 事件触发时将调用的回调，返回媒体密钥系统实例，适用于多个媒体密钥系统场景。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param eventType 事件类型。
 * @param info 从媒体密钥系统获取的事件信息。
 * @param infoLen 事件信息长度。
 * @param extra 从媒体密钥系统获得的额外信息。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数无效。
 * @since 12
 */
typedef Drm_ErrCode (*OH_MediaKeySystem_Callback)(MediaKeySystem *mediaKeySystem, DRM_EventType eventType,
    uint8_t *info, int32_t infoLen, char *extra);

/**
 * @brief 设置媒体密钥系统事件回调。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param callback 将回调设置为媒体密钥系统。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效。
 * @since 12
 */
Drm_ErrCode OH_MediaKeySystem_SetCallback(MediaKeySystem *mediaKeySystem, OH_MediaKeySystem_Callback callback);

/**
 * @brief 查询是否支持媒体密钥系统。
 * @param name 用于指向数字权限管理解决方案。
 * @return 是否支持。
 * @since 11
 * @version 1.0
 */
bool OH_MediaKeySystem_IsSupported(const char *name);

/**
 * @brief 查询是否支持媒体密钥系统。
 * @param name 用于指向数字权限管理解决方案。
 * @param mimeType 用于指定媒体类型。
 * @return 是否支持。
 * @since 11
 * @version 1.0
 */
bool OH_MediaKeySystem_IsSupported2(const char *name, const char *mimeType);

/**
 * @brief 查询是否支持媒体密钥系统。
 * @param name 用于指向数字权限管理解决方案。
 * @param mimeType 用于指定媒体类型。
 * @param contentProtectionLevel 用于指定安全等级。
 * @return 是否支持。
 * @since 11
 * @version 1.0
 */
bool OH_MediaKeySystem_IsSupported3(const char *name, const char *mimeType,
    DRM_ContentProtectionLevel contentProtectionLevel);

/**
 * @brief 获取支持的媒体密钥系统的名称和uuid。
 * @param infos 用于保存媒体密钥系统的名称和uuid的数组。
 * @param count 用于指示结构体DRM_MediaKeySystemMapInfo的计数。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}可能原因：1.输入参数infos为空指针或输入参数count为空指针；2.输入参数infos长度不足；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 12
 * @version 1.0
 */
Drm_ErrCode  OH_MediaKeySystem_GetMediaKeySystems(DRM_MediaKeySystemDescription *infos, uint32_t *count);

/**
 * @brief 根据名称创建媒体密钥系统实例。
 * @param name 说明将按名称创建哪个drm系统。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @return 当参数检查失败时返回DRM_ERR_INVALID_VAL，当函数调用成功时返回DRM_ERR_OK，
 * 当达到媒体密钥系统的最大数量时，返回DRM_ERR_MAX_SYSTEM_NUM_REACHED。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}可能原因：1.输入参数name为空指针或长度为0；2.输入参数mediaKeySystem为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息；\n
 *         {@link DRM_ERR_SERVICE_DIED}服务死亡；\n
 *         {@link DRM_ERR_MAX_SYSTEM_NUM_REACHED}已创建的MediaKeySystem数量达到最大限制(64个)。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_Create(const char *name, MediaKeySystem **mediaKeySystem);

/**
 * @brief 按字符串类型名称设置媒体密钥系统配置值。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param configName 配置名称字符串。
 * @param value 要设置的字符串的配置值。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，输入参数configName为空指针，或输入参数value为空指针。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_SetConfigurationString(MediaKeySystem *mediaKeySystem,
    const char *configName, const char *value);

/**
 * @brief 按字符串类型名称获取媒体密钥系统配置值。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param configName 字符串类型配置名。
 * @param value 字符串形式配置值。
 * @param valueLen 字符串形式配置值长度。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，输入参数configName为空指针，或输入参数value为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetConfigurationString(MediaKeySystem *mediaKeySystem,
    const char *configName, char *value, int32_t valueLen);

/**
 * @brief 通过字符数组类型配置名设置MediaKeySystem的配置值。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param configName 字符数组类型配置名。
 * @param value 字节数组形式配置值。
 * @param valueLen 字节数组形式配置值长度。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，输入参数configName为空指针，或输入参数value为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_SetConfigurationByteArray(MediaKeySystem *mediaKeySystem,
    const char *configName, uint8_t *value, int32_t valueLen);

/**
 * @brief 按字符数组类型名称获取媒体密钥系统配置值。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param configName 字符数组类型配置名称。
 * @param value 要获取数组中的配置值。
 * @param valueLen 数据的配置值长度。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，输入参数configName为空指针，输入参数value为空指针，或valueLen为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetConfigurationByteArray(MediaKeySystem *mediaKeySystem,
    const char *configName, uint8_t *value, int32_t *valueLen);

/**
 * @brief 获取媒体密钥系统度量信息。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param statistics 已获取度量信息。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数statistics为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetStatistics(MediaKeySystem *mediaKeySystem, DRM_Statistics *statistics);

/**
 * @brief 获取支持的最高内容保护级别的媒体密钥系统。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param contentProtectionLevel 内容保护级别。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数contentProtectionLevel为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetMaxContentProtectionLevel(MediaKeySystem *mediaKeySystem,
    DRM_ContentProtectionLevel *contentProtectionLevel);

/**
 * @brief 设置媒体密钥系统事件回调。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param callback 将回调设置为媒体密钥系统。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_SetMediaKeySystemCallback(MediaKeySystem *mediaKeySystem,
    MediaKeySystem_Callback callback);

/**
 * @brief 创建媒体密钥会话实例。
 * @param mediaKeySystem 将创建媒体密钥会话的媒体密钥系统实例。
 * @param level 指定内容保护级别。
 * @param mediaKeySession 媒体密钥会话实例。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数level超出合理范围，或mediaKeySession为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息；\n
 *         {@link DRM_ERR_SERVICE_DIED}服务死亡；\n
 *         {@link DRM_ERR_MAX_SESSION_NUM_REACHED}当前MediaKeySystem已创建的MediaKeySession数量达到最大限制(64个)。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_CreateMediaKeySession(MediaKeySystem *mediaKeySystem,
    DRM_ContentProtectionLevel *level, MediaKeySession **mediaKeySession);

/**
 * @brief 生成媒体密钥系统提供请求。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param request 发送给设备服务器的请求。
 * @param requestLen 设备证书请求的长度。
 * @param defaultUrl 设备证书服务器的网址。
 * @param defaultUrlLen 设备证书服务器的网址长度。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或其它指针类型输入参数为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GenerateKeySystemRequest(MediaKeySystem *mediaKeySystem, uint8_t *request,
    int32_t *requestLen, char *defaultUrl, int32_t defaultUrlLen);

/**
 * @brief 处理媒体密钥系统提供响应。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param response 将处理的响应。
 * @param responseLen 响应长度.
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数response为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_ProcessKeySystemResponse(MediaKeySystem *mediaKeySystem,
    uint8_t *response, int32_t responseLen);

/**
 * @brief 获取离线媒体密钥ID。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param offlineMediaKeyIds 所有离线媒体密钥的媒体密钥ID。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_NO_MEMORY}内存不足，内存分配失败；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数offlineMediaKeyIds为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetOfflineMediaKeyIds(MediaKeySystem *mediaKeySystem,
    DRM_OfflineMediakeyIdArray *offlineMediaKeyIds);

/**
 * @brief Get offline media key status.
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param offlineMediaKeyId 离线媒体密钥标识符。
 * @param offlineMediaKeyIdLen 离线媒体密钥标识符长度。
 * @param status 已获取媒体密钥状态。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或其它指针类型输入参数为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetOfflineMediaKeyStatus(MediaKeySystem *mediaKeySystem,
    uint8_t *offlineMediaKeyId, int32_t offlineMediaKeyIdLen, DRM_OfflineMediaKeyStatus *status);

/**
 * @brief 按id清除离线媒体密钥。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param offlineMediaKeyId 离线媒体密钥标识符。
 * @param offlineMediaKeyIdLen 离线媒体密钥标识符长度。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数offlineMediaKeyId为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_ClearOfflineMediaKeys(MediaKeySystem *mediaKeySystem,
    uint8_t *offlineMediaKeyId, int32_t offlineMediaKeyIdLen);

/**
 * @brief 获取媒体密钥系统的证书状态。
 * @param mediaKeySystem 媒体密钥系统实例。
 * @param certStatus 获得的证书状态值。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效，或输入参数certStatus为空指针；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_GetCertificateStatus(MediaKeySystem *mediaKeySystem,
    DRM_CertificateStatus *certStatus);

/**
 * @brief Destroy a 媒体密钥系统实例。
 * @param mediaKeySystem 指定将销毁哪个媒体密钥系统实例。
 * @return 函数结果代码：
 *         {@link DRM_ERR_OK}执行成功；\n
 *         {@link DRM_ERR_INVALID_VAL}输入参数mediaKeySystem为空指针或无效；\n
 *         {@link DRM_ERR_UNKNOWN}发生内部错误，请查看日志详细信息。
 * @since 11
 * @version 1.0
 */
Drm_ErrCode OH_MediaKeySystem_Destroy(MediaKeySystem *mediaKeySystem);

#ifdef __cplusplus
}
#endif

#endif // OHOS_DRM_NATIVE_MEDIA_KEY_SYSTEM_H
