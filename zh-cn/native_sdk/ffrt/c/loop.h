/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 *
 * @since 12
 */
 
/**
 * @file loop.h
 *
 * @brief 声明FFRT LOOP机制的C接口。
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 12
 */

#ifndef FFRT_API_C_LOOP_H
#define FFRT_API_C_LOOP_H

#include "queue.h"
#include "type_def.h"

typedef void* ffrt_loop_t;

/**
 * @brief 创建loop队列。
 * 
 * @param queue 并发队列。
 * @return 创建成功返回ffrt_loop_t对象，失败返回空指针。
 * @since 12
*/
FFRT_C_API ffrt_loop_t ffrt_loop_create(ffrt_queue_t queue);

/**
 * @brief 销毁loop队对象。
 * 
 * @param loop loop对象。
 * @return 销毁成功返回0，-1是销毁失败。
 * @since 12
*/
FFRT_C_API int ffrt_loop_destroy(ffrt_loop_t loop);

/**
 * @brief 开启loop循环。
 * 
 * @param loop loop对象。
 * @return loop循环失败返回-1，0是成功。
 * @since 12
*/
FFRT_C_API int ffrt_loop_run(ffrt_loop_t loop);

/**
 * @brief 停止loop循环。
 * 
 * @param loop loop对象。
 * @since 12
*/
FFRT_C_API void ffrt_loop_stop(ffrt_loop_t loop);

/**
 * @brief 管理loop上的监听事件。
 * 
 * @param loop loop对象。
 * @param op fd操作符。
 * @param fd 事件描述符。
 * @param events 事件。
 * @param data 事件变化时触发的回调函数的入参。
 * @param cb 事件变化时触发的回调函数。
 * @return 成功返回0，失败返回-1.
 * @since 12
*/
FFRT_C_API int ffrt_loop_epoll_ctl(ffrt_loop_t loop, int op, int fd, uint32_t events, void* data, ffrt_poller_cb cb);

/**
 * @brief 在ffrt loop上启动定时器。
 * 
 * @param loop loop对象。
 * @param timeout 超时时间.
 * @param data 事件变化时触发的回调函数的入参。
 * @param cb 事件变化时触发的回调函数。
 * @param repeat 是否重复执行该定时器（该功能暂未支持）。
 * @return 返回定时器句柄。
 * @since 12
*/
FFRT_C_API ffrt_timer_t ffrt_loop_timer_start(ffrt_loop_t loop, uint64_t timeout, void* data, ffrt_timer_cb cb, bool repeat);

/**
 * @brief 停止ffrt loop定时器。
 * 
 * @param loop loop对象。
 * @param handle timer对象。
 * @return 成功返回0，失败返回-1.
 * @since 12
*/
FFRT_C_API int ffrt_loop_timer_stop(ffrt_loop_t loop, ffrt_timer_t handle);

#endif
