/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MindSpore
 * @{
 *
 * @brief 提供MindSpore Lite的模型推理相关接口，该模块下的接口是非线程安全的。
 *
 * @Syscap SystemCapability.Ai.MindSpore
 * @since 9
 */

/**
 * @file data_type.h
 *
 * @brief 声明了张量的数据的类型。
 *
 * 引用文件<mindspore/data_type.h>
 * @library libmindspore_lite_ndk.so
 * @since 9
 */
#ifndef MINDSPORE_INCLUDE_C_API_DATA_TYPE_C_H
#define MINDSPORE_INCLUDE_C_API_DATA_TYPE_C_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief MSTensor保存的数据支持的类型。
 *
 * @since 9
 */
typedef enum OH_AI_DataType {
  /** 未知的数据类型 */
  OH_AI_DATATYPE_UNKNOWN = 0,
  /** String数据类型 */
  OH_AI_DATATYPE_OBJECTTYPE_STRING = 12,
  /** List数据类型 */
  OH_AI_DATATYPE_OBJECTTYPE_LIST = 13,
  /** Tuple数据类型 */
  OH_AI_DATATYPE_OBJECTTYPE_TUPLE = 14,
  /** TensorList数据类型 */
  OH_AI_DATATYPE_OBJECTTYPE_TENSOR = 17,
  /** Number类型的起始 */
  OH_AI_DATATYPE_NUMBERTYPE_BEGIN = 29,
  /** Bool数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_BOOL = 30,
  /** Int8数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_INT8 = 32,
  /** 表示Int16数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_INT16 = 33,
  /** 表示Int32数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_INT32 = 34,
  /** 表示Int64数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_INT64 = 35,
  /** 表示UInt8数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_UINT8 = 37,
  /** 表示UInt16数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_UINT16 = 38,
  /** 表示UInt32数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_UINT32 = 39,
  /** 表示UInt64数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_UINT64 = 40,
  /** 表示Float16数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_FLOAT16 = 42,
  /** 表示Float32数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_FLOAT32 = 43,
  /** 表示Float64数据类型 */
  OH_AI_DATATYPE_NUMBERTYPE_FLOAT64 = 44,
  /** 表示Number类型的结尾 */
  OH_AI_DATATYPE_NUMBERTYPE_END = 46,
  /** 表示无效的数据类型 */
  OH_AI_DataTypeInvalid = INT32_MAX,
} OH_AI_DataType;

#ifdef __cplusplus
}
#endif

/** @} */
#endif // MINDSPORE_INCLUDE_C_API_DATA_TYPE_C_H
