/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
#ifndef OHOS_INPUTMETHOD_INPUTMETHOD_PROXY_CAPI_H
#define OHOS_INPUTMETHOD_INPUTMETHOD_PROXY_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_inputmethod_proxy_capi.h
 *
 * @brief 提供使用输入法的方法，可以向输入法应用发送请求和通知。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#include <stddef.h>

#include "inputmethod_types_capi.h"
#include "inputmethod_cursor_info_capi.h"
#include "inputmethod_private_command_capi.h"
#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */
/**
 * @brief 输入法代理对象。
 *
 * 使用此对象可以用于调用使用输入法的方法。
 *
 * @since 12
 */
typedef struct InputMethod_InputMethodProxy InputMethod_InputMethodProxy;

/**
 * @brief 显示键盘。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_DETACHED} - 未绑定输入法。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodProxy_ShowKeyboard(InputMethod_InputMethodProxy *inputMethodProxy);

/**
 * @brief 隐藏键盘。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_DETACHED} - 未绑定输入法。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodProxy_HideKeyboard(InputMethod_InputMethodProxy *inputMethodProxy);

/**
 * @brief 通知文本框选区变化。
 *
 * 当输入框内文本内容、光标位置或选中文本发生变化时，通过此接口将信息通知给输入法应用。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @param text 整个输入文本。
 * @param length text参数的长度。最大长度为8K。
 * @param start 所选文本的起始位置。
 * @param end 所选文本的结束位置。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_PARAMCHECK} - 表示参数错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_DETACHED} - 未绑定输入法。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodProxy_NotifySelectionChange(
    InputMethod_InputMethodProxy *inputMethodProxy, char16_t text[], size_t length, int start, int end);

/**
 * @brief 通知输入框配置变化。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @param enterKey 回车键类型。
 * @param textType 输入框类型。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_PARAMCHECK} - 表示参数错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_DETACHED} - 未绑定输入法。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodProxy_NotifyConfigurationChange(InputMethod_InputMethodProxy *inputMethodProxy,
    InputMethod_EnterKeyType enterKey, InputMethod_TextInputType textType);

/**
 * @brief 通知光标位置变化。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @param cursorInfo 指向{@link InputMethod_CursorInfo}实例的指针，表示光标信息。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_PARAMCHECK} - 表示参数错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_DETACHED} - 未绑定输入法。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodProxy_NotifyCursorUpdate(
    InputMethod_InputMethodProxy *inputMethodProxy, InputMethod_CursorInfo *cursorInfo);

/**
 * @brief 发送私有数据命令。
 *
 * @param inputMethodProxy 表示指向{@link InputMethod_InputMethodProxy}实例的指针。
 *     inputMethodProxy由调用{@link OH_InputMethodController_Attach}获取。
 * @param privateCommand 私有命令, 定义在{@link InputMethod_PrivateCommand}，最大大小为32KB。
 * @param size 私有命令的大小. 最大大小为5。
 * @return 返回一个特定的错误码。
 *     {@link InputMethod_ErrorCode#IME_ERR_OK} - 表示成功。
 *     {@link InputMethod_ErrorCode#IME_ERR_PARAMCHECK} - 表示参数错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMCLIENT} - 输入法客户端错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_IMMS} - 输入法服务错误。
 *     {@link InputMethod_ErrorCode#IME_ERR_DETACHED} - 未绑定输入法。
 *     {@link InputMethod_ErrorCode#IME_ERR_NULL_POINTER} - 非预期的空指针。
 * 具体错误码可以参考{@link InputMethod_ErrorCode}。
 * @since 12
 */
InputMethod_ErrorCode OH_InputMethodProxy_SendPrivateCommand(
    InputMethod_InputMethodProxy *inputMethodProxy, InputMethod_PrivateCommand *privateCommand[], size_t size);
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // INPUTMETHOD_INPUTMETHOD_PROXY_CAP_H