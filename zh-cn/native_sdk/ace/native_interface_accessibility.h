/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
/**
 * @addtogroup ArkUI_Accessibility
 * @{
 *
 * @brief 描述ArkUI Accessibilit对外支持的Native能力，如查询无障碍节点、上报无障碍事件等。
 *
 * @since 13
 */

/**
 * @file native_interface_accessibility.h
 *
 * @brief 声明用于访问Native Accessibility的API。
 *
 * @since 13
 */
#ifndef _NATIVE_INTERFACE_ACCESSIBILITY_H
#define _NATIVE_INTERFACE_ACCESSIBILITY_H

#include <cstdint>

#ifdef __cplusplus
extern "C"{
#endif

/**
 * @brief 提供封装的ArkUI_AccessibilityElementInfo实例。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityElementInfo ArkUI_AccessibilityElementInfo;

/**
 * @brief 定义Accessibility事件信息。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityEventInfo ArkUI_AccessibilityEventInfo;

/**
 * @brief 定义Accessibility本地提供者。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityProvider ArkUI_AccessibilityProvider;

/**
 * @brief 提供一个封装的ArkUI_AccessibilityActionArguments实例。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityActionArguments  ArkUI_AccessibilityActionArguments;

/**
 * @brief Accessibility操作类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_INVALID = 0,
    /** 收到事件后，组件需要对点击做出响应。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_CLICK = 0x00000010,
    /** 收到事件后，组件需要对长点击做出响应。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_LONG_CLICK = 0x00000020,
    /** 表示获取辅助功能焦点的操作，特定组件已聚焦。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_ACCESSIBILITY_FOCUS = 0x00000040,
    /** 表示清除辅助功能焦点的操作。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_CLEAR_ACCESSIBILITY_FOCUS = 0x00000080,
    /** 滚动组件响应向前滚动动作。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SCROLL_FORWARD = 0x00000100,
    /** 滚动组件响应反向滚动操作。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SCROLL_BACKWARD = 0x00000200,
    /** 复制文本组件的选定内容。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_COPY = 0x00000400,
    /** 粘贴文本组件的选定内容。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_PASTE = 0x00000800,
    /** 剪切文本组件的选定内容。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_CUT = 0x00001000,
    /** 表示选择操作，需要设置selectTextBegin、TextEnd和TextInForWard参数，在编辑框中选择文本段。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SET_SELECTION = 0x00002000,
    /** 设置文本组件的文本内容。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SET_TEXT = 0x00004000,
    /** 设置文本组件的光标位置。 */
    ARKUI_NATIVE_ACCESSIBILITY_ACTION_SET_CURSOR_POSITION = 0x00100000,
} ArkUI_Accessibility_ActionType;

/**
 * @brief Accessibility事件类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_INVALID = 0,
    /** 点击事件，在UI组件响应后发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_CLICKED_EVENT = 0x00000001,
    /** 长点击事件，在UI组件响应后发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_LONG_CLICKED_EVENT = 0x00000002,
    /** 被选中事件，控件响应完成后发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_SELECTED_EVENT = 0x00000004,
    /** 文本更新事件，需要在文本更新时发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_TEXT_UPDATE_EVENT = 0x00000010,
    /** 页面更新事件，当页面跳转、切换、大小更改或移动时发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_STATE_UPDATE = 0x00000020,
    /** 页面内容发生变化时需要发送事件。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_CONTENT_UPDATE = 0x00000800,
    /** scrolled事件，当可滚动的组件上发生滚动事件时，会发送此事件。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_SCROLLED_EVENT = 0x000001000,
    /** Accessibility焦点事件，在UI组件响应后发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_ACCESSIBILITY_FOCUSED_EVENT = 0x00008000,
    /** Accessibility焦点清除事件，在UI组件响应后发送。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED_EVENT = 0x00010000,
    /** 主动请求指定节点聚焦。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_REQUEST_FOCUS_FOR_ACCESSIBILITY = 0x02000000,
    /** UI组件上报页面打开事件。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_OPEN = 0x20000000,
    /** UI组件上报页面关闭事件。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_PAGE_CLOSE = 0x08000000,
    /** 广播Accessibility事件，请求主动播放指定的内容事件。 */
    ARKUI_NATIVE_ACCESSIBILITY_TYPE_VIEW_ANNOUNCE_FOR_ACCESSIBILITY = 0x10000000,
} ArkUI_AccessibilityEventType;

/**
 * @brief 定义Accessible操作结构体。
 *
 * @since 13
 */
typedef struct {
    /** 操作类型。 */
    ArkUI_Accessibility_ActionType actionType;
    /** 操作描述信息。 */
    const char* description;
} ArkUI_AccessibleAction;

/**
 * @brief 定义Accessible区域。
 *
 * @since 13
 */
typedef struct {
    /** 左上角x像素坐标。 */
    int32_t leftTopX;
    /** 左上角y像素坐标。 */
    int32_t leftTopY;
    /** 右下角x像素坐标。 */
    int32_t rightBottomX;
    /** 右下角y像素坐标。 */
    int32_t rightBottomY;
} ArkUI_AccessibleRect;

/**
 * @brief 定义Accessible范围信息。
 *
 * @since 13
 */
typedef struct {
    /** 最小值。 */
    double min;
    /** 最大值。 */
    double max;
    /** 当前值。 */
    double current;
} ArkUI_AccessibleRangeInfo;

/**
 * @brief 定义accessible网格信息。
 *
 * @since 13
 */
typedef struct {
    /** 行数。 */
    int32_t rowCount;
    /** 列数。 */
    int32_t columnCount;
    /** 0: 仅选择一行，否则选择多行。 */
    int32_t selectionMode;
} ArkUI_AccessibleGridInfo;

/**
 * @brief 定义accessible网格项信息。
 *
 * @since 13
 */
typedef struct {
    /** 是否头部。 */
    bool heading;
    /** 是否选择。 */
    bool selected;
    /** 列数。 */
    int32_t columnIndex;
    /** 行数。 */
    int32_t rowIndex;
    /** 列间距。 */
    int32_t columnSpan;
    /** 行间距。 */
    int32_t rowSpan;
} ArkUI_AccessibleGridItemInfo;

/**
 * @brief Accessibility错误代码状态的枚举。
 *
 * @since 13
 */
enum AcessbilityErrorCode{
    /** 成功。 */
    OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS = 0,
    /** 失败。 */
    OH_ARKUI_ACCESSIBILITY_RESULT_FAILED = -1,
    /** 无效参数。 */
    OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER = -2,
    /** 内存超出。 */
    OH_ARKUI_ACCESSIBILITY_RESULT_OUT_OF_MEMORY = -3,
} ;

/**
 * @brief Accessibility搜索类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 查询父节点。 */
    NATIVE_SEARCH_MODE_PREFETCH_PREDECESSORS = 1 << 0,
    /** 查询兄弟节点。 */
    NATIVE_SEARCH_MODE_PREFETCH_SIBLINGS = 1 << 1,
    /** 查询下一层孩子节点。 */
    NATIVE_SEARCH_MODE_PREFETCH_CHILDREN = 1 << 2,
    /** 查询所有孩子节点。 */
    NATIVE_SEARCH_MODE_PREFETCH_RECURSIVE_CHILDREN = 1 << 3,
} ArkUI_AccessibilitySearchMode;

/**
 * @brief Accessibility焦点类型的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    NATIVE_FOCUS_TYPE_INVALID = -1,
    /** 输入焦点类型。 */
    NATIVE_FOCUS_TYPE_INPUT = 1 << 0,
    /** Accessibility焦点类型。 */
    NATIVE_FOCUS_TYPE_ACCESSIBILITY = 1 << 1,
} ArkUI_AccessibilityFocusType;

/**
 * @brief Accessibility焦点移动方向的枚举。
 *
 * @since 13
 */
typedef enum {
    /** 无效。 */
    NATIVE_DIRECTION_INVALID = 0,
    /** 上。 */
    NATIVE_DIRECTION_UP = 0x00000001,
    /** 下。 */
    NATIVE_DIRECTION_DOWN = 0x00000002,
    /** 左。 */
    NATIVE_DIRECTION_LEFT = 0x00000004,
    /** 右。 */
    NATIVE_DIRECTION_RIGHT = 0x00000008,
    /** 前进。 */
    NATIVE_DIRECTION_FORWARD = 0x00000010,
    /** 后退。 */
    NATIVE_DIRECTION_BACKWARD = 0x00000020,
} ArkUI_AccessibilityFocusMoveDirection;

/**
 * @brief 提供封装的ArkUI_AccessibilityElementInfoList实例。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityElementInfoList ArkUI_AccessibilityElementInfoList;

/**
 * @brief 注册Accessibility提供程序回调。
 *
 * @since 13
 */
typedef struct ArkUI_AccessibilityProviderCallbacks {
    /** 当需要基于指定节点获取元素信息时调用。 */
    int32_t (*FindAccessibilityNodeInfosById)(int64_t elementId, ArkUI_AccessibilitySearchMode mode,
        int32_t requestId, ArkUI_AccessibilityElementInfoList* elementList);
    /** 当需要基于指定节点和文本内容获取元素信息时调用。 */
    int32_t (*FindAccessibilityNodeInfosByText)(int64_t elementId, const char* text, int32_t requestId,
        ArkUI_AccessibilityElementInfoList* elementList);
    /** 当需要基于指定节点获取焦点元素信息时调用。 */
    int32_t (*FindFocusedAccessibilityNode)(int64_t elementId, ArkUI_AccessibilityFocusType focusType,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementinfo);
    /** 根据参考节点查询可以聚焦的节点，根据模式和方向查询下一个可以聚焦的节点。 */
    int32_t (*FindNextFocusAccessibilityNode)(
        int64_t elementId, ArkUI_AccessibilityFocusMoveDirection direction,
        int32_t requestId, ArkUI_AccessibilityElementInfo* elementList);
    /** 在指定节点上执行Action操作。 */
    int32_t (*ExecuteAccessibilityAction)(int64_t elementId, ArkUI_Accessibility_ActionType action,
        ArkUI_AccessibilityActionArguments  actionArguments, int32_t requestId);
    /** 清除当前焦点节点的焦点状态。 */
    int32_t (*ClearFocusedFocusAccessibilityNode)();
    /** 查询指定节点的当前光标位置。 */
    int32_t (*GetAccessibilityNodeCursorPosition)(int64_t elementId, int32_t requestId, int32_t* index);
} ArkUI_AccessibilityProviderCallbacks;

/**
 * @brief 为此ArkUI_AccessibilityProvider实例注册回调。
 *
 * @param provider 表示指向ArkUI_AccessibilityProvider实例的指针。
 * @param callbacks 表示指向GetAccessibilityNodeCursorPosition回调的指针。
 * @return 返回执行的错误码。
 * @since 13
 */
int32_t OH_ArkUI_AccessibilityProviderRegisterCallback(
    ArkUI_AccessibilityProvider* provider, ArkUI_AccessibilityProviderCallbacks* callbacks); 

/**
 * @brief 发送accessibility事件信息。
 * 
 * @param provider 表示指向ArkUI_AccessibilityProvider实例的指针。
 * @param eventInfo 表示指向Accessibility事件信息的指针。
 * @param callback 表示指向SendAccessibilityAsyncEvent回调。
 * @since 13
 */
void OH_ArkUI_SendAccessibilityAsyncEvent(
    ArkUI_AccessibilityProvider* provider, ArkUI_AccessibilityEventInfo* eventInfo,
    void (*callback)(int32_t errorCode));

/**
 * @brief 添加并获取ArkUI_AccessibilityElementInfo指针。
 * 
 * @param list 表示指向ArkUI_AccessibilityElementInfoList指针。
 * @return 返回表示指向ArkUI_AccessibilityElementInfo指针。
 * @since 13
 */
ArkUI_AccessibilityElementInfo* OH_ArkUI_AddAndGetAccessibilityElementInfo(
    ArkUI_AccessibilityElementInfoList* list);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置pageId。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param pageId 表示pageId。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoPageId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t pageId);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置componentId。
*
* @param elementInfo ArkUI_AccessibilityElementInfo指针。
* @param componentId 表示componentId。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoComponentId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t componentId);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置parentId。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param parentId 表示parentId。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoParentId(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t parentId);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置componentType。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param componentType 表示componentType。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoComponentType(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* componentType);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置组件内容。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param contents 表示组件内容。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoContents(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* contents);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置提示文本。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param hintText 表示提示文本。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoHintText(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* hintText);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置Accessibility文本。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityText 表示Accessibility文本。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityText(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityText);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置Accessibility描述信息。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityDescription 表示Accessibility描述信息。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityDescription(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityDescription);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置childCount和childNodeIds。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param childCount 表示孩子节点数量。
* @param childNodeIds 表示孩子节点id集合。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoChildNodeIds(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t childCount, int64_t* childNodeIds);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置operationActions。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param operationActions 表示operationActions。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoOperationActions(ArkUI_AccessibilityElementInfo* elementInfo,
    int32_t operationCount, ArkUI_AccessibleAction* operationActions);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置屏幕区域。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param screenRect 表示屏幕区域。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoScreenRect(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleRect* screenRect);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置checkable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param checkable 表示checkable。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoCheckable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool checkable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置checked。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param checked 表示checked。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoChecked(
    ArkUI_AccessibilityElementInfo* elementInfo, bool checked);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置是否可获焦。
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param focusable 表示是否可获焦。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoFocusable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool focusable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置是否获焦。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isFocused 表示是否获焦。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoFocused(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isFocused);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置是否可见。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isVisible 表示是否可见。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoVisible(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isVisible);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置accessibilityFocused。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityFocused 表示accessibilityFocused。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityFocused(
    ArkUI_AccessibilityElementInfo* elementInfo, bool accessibilityFocused);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置selected。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param selected 表示selected。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, bool selected);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置clickable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param clickable 表示clickable。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoClickable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool clickable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置longClickable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param longClickable 表示longClickable。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoLongClickable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool longClickable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置isEnable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isEnable 表示是否允许。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoEnabled(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isEnabled);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置isPassword。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isPassword 表示isPassword。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoIsPassword(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isPassword);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置scrollable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param scrollable 表示scrollable。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoScrollable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool scrollable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置editable。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param editable 表示editable。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoEditable(
    ArkUI_AccessibilityElementInfo* elementInfo, bool editable);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置isHint。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param isHint 表示isHint。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoIsHint(
    ArkUI_AccessibilityElementInfo* elementInfo, bool isHint);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置rangeInfo。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param rangeInfo 表示rangeInfo。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoRangeInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleRangeInfo* rangeInfo);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置gridInfo。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param gridInfo 表示gridInfo。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoGridInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleGridInfo* gridInfo);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置gridItem。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param gridItem 表示gridItem。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoGridItemInfo(
    ArkUI_AccessibilityElementInfo* elementInfo, ArkUI_AccessibleGridItemInfo* gridItem);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置textBeginSelected。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param textBeginSelected 表示textBeginSelected。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoTextBeginSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t textBeginSelected);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置textEndSelected。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param textEndSelected 表示textEndSelected。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoTextEndSelected(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t textEndSelected);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置currentItemIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param currentItemIndex 表示currentItemIndex。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoCurrentItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t currentItemIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置beginItemIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param beginItemIndex 表示beginItemIndex。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBeginItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t beginItemIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置endItemIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param endItemIndex 表示endItemIndex。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoEndItemIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t endItemIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置itemCount。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param itemCount 表示itemCount。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoItemCount(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t itemCount);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置offset。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param offset 表示相对于元素顶部坐标的滚动像素偏移。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityOffset(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t offset);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置accessibilityGroup。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityGroup 表示accessibilityGroup。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityGroup(
    ArkUI_AccessibilityElementInfo* elementInfo, bool accessibilityGroup);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置accessibilityLevel。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param accessibilityLevel 表示accessibilityLevel。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityLevel(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* accessibilityLevel);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置zIndex。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param zIndex 表示zIndex。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoZIndex(
    ArkUI_AccessibilityElementInfo* elementInfo, int32_t zIndex);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置opacity。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param opacity 表示opacity。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoAccessibilityOpacity(
    ArkUI_AccessibilityElementInfo* elementInfo, float opacity);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置backgroundColor。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param backgroundColor 表示backgroundColor。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBackgroundColor(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* backgroundColor);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置backgroundImage。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param backgroundImage 表示backgroundImage。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBackgroundImage(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* backgroundImage);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置blur。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param blur 表示blur。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoBlur(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* blur);

/**
* @brief 为ArkUI_AccessibilityElementInfo设置hitTestBehavior。
*
* @param elementInfo 表示指向ArkUI_AccessibilityElementInfo指针。
* @param hitTestBehavior 表示hitTestBehavior。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityElementInfoHitTestBehavior(
    ArkUI_AccessibilityElementInfo* elementInfo, const char* hitTestBehavior);

/**
 * @brief 创建一个ArkUI_AccessibilityEventInfo对象。
 *
 * @return Returns ArkUI_AccessibilityEventInfo对象。
 * @since 13
 */
ArkUI_AccessibilityEventInfo* OH_ArkUI_CreateAccessibilityEventInfo(void);

/**
 * @brief 销毁ArkUI_AccessibilityEventInfo对象。
 *
 * @param eventInfo 需要被销毁的ArkUI_AccessibilityEventInfo对象。
 * @since 13
 */
void OH_ArkUI_DestoryAccessibilityEventInfo(ArkUI_AccessibilityEventInfo* eventInfo);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置事件类型。
*
* @param eventInfo 表示事件信息。
* @param eventType 表示事件类型。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventEventType(
    ArkUI_AccessibilityEventInfo* eventInfo,  ArkUI_AccessibilityEventType eventType);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置pageId。
*
* @param eventInfo 表示事件信息。
* @param pageId 表示pageId。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventPageId(
    ArkUI_AccessibilityEventInfo* eventInfo,  int32_t pageId);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置textAnnouncedForAccessibility。
*
* @param eventInfo 表示事件信息。
* @param textAnnouncedForAccessibility 表示textAnnouncedForAccessibility。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventTextAnnouncedForAccessibility(
    ArkUI_AccessibilityEventInfo* eventInfo,  const char* textAnnouncedForAccessibility);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置requestFocusId。
*
* @param eventInfo 表示事件信息。
* @param requestFocusId 表示请求焦点id。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventRequestFocusId(
    ArkUI_AccessibilityEventInfo* eventInfo,  int32_t requestFocusId);

/**
* @brief 为ArkUI_AccessibilityEventInfo设置elementInfo。
*
* @param eventInfo 表示事件信息。
* @param elementInfo 表示ArkUI_AccessibilityElementInfo元素信息。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_SetAccessibilityEventElementInfo(
    ArkUI_AccessibilityEventInfo* eventInfo,  ArkUI_AccessibilityElementInfo* elementInfo);

/**
* @brief 通过key从ArkUI_AccessibilityActionArguments获取值。
*
* @param arguments 表示ArkUI_AccessibilityActionArguments对象信息。
* @param key 表示key。
* @param value 表示value。
* @return 成功返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_SUCCESS}。
*         参数错误返回 {@link OH_ARKUI_ACCESSIBILITY_RESULT_BAD_PARAMETER}。
* @since 13
*/
int32_t OH_ArkUI_FindAccessibilityActionArgumentByKey(
    ArkUI_AccessibilityActionArguments* arguments, const char* key, char* value);
#ifdef __cplusplus
};
#endif
#endif // _NATIVE_INTERFACE_ACCESSIBILITY_H
