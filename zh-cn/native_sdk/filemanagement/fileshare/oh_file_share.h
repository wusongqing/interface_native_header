/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILE_MANAGEMENT_OH_FILE_SHARE_H
#define FILE_MANAGEMENT_OH_FILE_SHARE_H

#include "error_code.h"

/**
 * @addtogroup fileShare
 * @{
 *
 * @brief 此模块提供文件分享功能，以授权对其他应用程序具有读写权限的公共目录文件的统一资源标识符（URI）。
 * @since 12
 */

/**
 * @file oh_file_share.h
 *
 * @brief 提供基于URI的文件及目录授于持久化权限、权限激活、权限查询等方法。
 * @library libohfileshare.so
 * @syscap SystemCapability.FileManagement.AppFileService.FolderAuthorization
 * @since 12
 */
#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief URI操作模式枚举值。
 *
 * @since 12
 */
typedef enum FileShare_OperationMode {
    /**
     * @brief 读取权限。
     */
    READ_MODE = 1 << 0,

    /**
     * @brief 写入权限。
     */
    WRITE_MODE = 1 << 1
} FileShare_OperationMode;

/**
 * @brief 授予或使能权限策略失败的URI对应的错误码枚举值。
 *
 * @since 12
 */
typedef enum FileShare_PolicyErrorCode {
    /**
     * @brief URI禁止被持久化。
     */
    PERSISTENCE_FORBIDDEN = 1,

    /**
     * @brief 无效的模式。
     */
    INVALID_MODE = 2,

    /**
     * @brief 无效路径。
     */
    INVALID_PATH = 3,

    /**
     * @brief 权限没有被持久化。
     */
    PERMISSION_NOT_PERSISTED = 4
} FileShare_PolicyErrorCode;

/**
 * @brief 授予或使能权限失败的URI策略结果。
 *
 * @since 12
 */
typedef struct FileShare_PolicyErrorResult {
    /**
     * 授予或使能策略失败的URI。
     */
    char *uri;

    /**
     * 授予或使能策略失败的URI对应的错误码。
     */
    FileShare_PolicyErrorCode code;

    /**
     * 授予或使能策略失败的URI对应的原因。
     */
    char *message;
} FileShare_PolicyErrorResult;

/**
 * @brief 需要授予或使能权限URI的策略信息。
 *
 * @since 12
 */
typedef struct FileShare_PolicyInfo {
    /**
     * 需要授予或使能权限的URI。
     */
    char *uri;

    /**
     * URI的字节长度。
     */
    unsigned int length;

    /**
     * 授予或使能权限的URI访问模式。
     * 示例：FileShare_OperationMode.READ_MODE 、 FileShare_OperationMode.WRITE_MODE
     * 或者 FileShare_OperationMode.READ_MODE|FileShare_OperationMode.WRITE_MODE。
     */
    unsigned int operationMode;
} FileShare_PolicyInfo;

/**
 * @brief 对所选择的多个文件或目录URI持久化授权。
 *
 * @permission ohos.permission.FILE_ACCESS_PERSIST
 * @param policies 一个指向FileShare_PolicyInfo实例的指针。
 * @param policyNum FileShare_PolicyInfo实例数组的大小。
 * @param result FileShare_PolicyErrorResult数组指针. 请使用OH_FileShare_ReleasePolicyErrorResult()进行资源释放。
 * @param resultNum FileShare_PolicyErrorResult数组大小。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 *         {@link E_PARAMS} 401 - 输入参数无效。可能的原因有：1.参数policies或参数result或参数resultNum为空指针;
 *             2.参数policyNum值为0或者超过最大长度(500);3.参数policies中携带的uri为空或者length为0或者uri的长度与length不一致。
 *         {@link E_DEVICE_NOT_SUPPORT} 801 - 当前设备类型不支持此接口。
 *         {@link E_PERMISSION} 201 - 接口权限校验失败。
 *         {@link E_ENOMEM} 13900011 - 分配或者拷贝内存失败。
 *         {@link E_EPERM} 13900001 - 操作不被允许。
 *         {@link E_UNKNOWN_ERROR} 13900042 - 内部未知错误，调用其它部件返回的除以上错误之外的其它错误。
 *         {@link E_NO_ERROR} 0 - 接口调用成功。
 * @since 12
 */
FileManagement_ErrCode OH_FileShare_PersistPermission(const FileShare_PolicyInfo *policies, unsigned int policyNum,
    FileShare_PolicyErrorResult **result, unsigned int *resultNum);

/**
 * @brief 对所选择的多个文件或目录uri取消持久化授权。
 *
 * @permission ohos.permission.FILE_ACCESS_PERSIST
 * @param policies 一个指向FileShare_PolicyInfo实例的指针。
 * @param policyNum FileShare_PolicyInfo实例数组的大小。
 * @param result FileShare_PolicyErrorResult数组指针. 请使用OH_FileShare_ReleasePolicyErrorResult()进行资源释放。
 * @param resultNum FileShare_PolicyErrorResult数组大小。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 *         {@link E_PARAMS} 401 - 输入参数无效。可能的原因有：1.参数policies或参数result或参数resultNum为空指针;
 *             2.参数policyNum值为0或者超过最大长度(500);3.参数policies中携带的uri为空或者length为0或者uri的长度与length不一致。
 *         {@link E_DEVICE_NOT_SUPPORT} 801 - 当前设备类型不支持此接口。
 *         {@link E_PERMISSION} 201 - 接口权限校验失败。
 *         {@link E_ENOMEM} 13900011 - 分配或者拷贝内存失败。
 *         {@link E_EPERM} 13900001 - 操作不被允许。
 *         {@link E_UNKNOWN_ERROR} 13900042 - 内部未知错误，调用其它部件返回的除以上错误之外的其它错误。
 *         {@link E_NO_ERROR} 0 - 接口调用成功。
 * @since 12
 */
FileManagement_ErrCode OH_FileShare_RevokePermission(const FileShare_PolicyInfo *policies, unsigned int policyNum,
    FileShare_PolicyErrorResult **result, unsigned int *resultNum);

/**
 * @brief 使能多个已经持久化授权的文件或目录。
 *
 * @permission ohos.permission.FILE_ACCESS_PERSIST
 * @param policies 一个指向FileShare_PolicyInfo实例的指针。
 * @param policyNum FileShare_PolicyInfo实例数组的大小。
 * @param result FileShare_PolicyErrorResult数组指针. 请使用OH_FileShare_ReleasePolicyErrorResult()进行资源释放。
 * @param resultNum FileShare_PolicyErrorResult数组大小。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 *         {@link E_PARAMS} 401 - 输入参数无效。可能的原因有：1.参数policies或参数result或参数resultNum为空指针;
 *             2.参数policyNum值为0或者超过最大长度(500);3.参数policies中携带的uri为空或者length为0或者uri的长度与length不一致。
 *         {@link E_DEVICE_NOT_SUPPORT} 801 - 当前设备类型不支持此接口。
 *         {@link E_PERMISSION} 201 - 接口权限校验失败。
 *         {@link E_ENOMEM} 13900011 - 分配或者拷贝内存失败。
 *         {@link E_EPERM} 13900001 - 操作不被允许。
 *         {@link E_UNKNOWN_ERROR} 13900042 - 内部未知错误，调用其它部件返回的除以上错误之外的其它错误。
 *         {@link E_NO_ERROR} 0 - 接口调用成功。
 * @since 12
 */
FileManagement_ErrCode OH_FileShare_ActivatePermission(const FileShare_PolicyInfo *policies, unsigned int policyNum,
    FileShare_PolicyErrorResult **result, unsigned int *resultNum);

/**
 * @brief 取消使能持久化授权过的多个文件或目录。
 *
 * @permission ohos.permission.FILE_ACCESS_PERSIST
 * @param policies 一个指向FileShare_PolicyInfo实例的指针。
 * @param policyNum FileShare_PolicyInfo实例数组的大小。
 * @param result FileShare_PolicyErrorResult数组指针. 请使用OH_FileShare_ReleasePolicyErrorResult()进行资源释放。
 * @param resultNum FileShare_PolicyErrorResult数组大小。
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 *         {@link E_PARAMS} 401 - 输入参数无效。可能的原因有：1.参数policies或参数result或参数resultNum为空指针;
 *             2.参数policyNum值为0或者超过最大长度(500);3.参数policies中携带的uri为空或者length为0或者uri的长度与length不一致。
 *         {@link E_DEVICE_NOT_SUPPORT} 801 - 当前设备类型不支持此接口。
 *         {@link E_PERMISSION} 201 - 接口权限校验失败。
 *         {@link E_ENOMEM} 13900011 - 分配或者拷贝内存失败。
 *         {@link E_EPERM} 13900001 - 操作不被允许。
 *         {@link E_UNKNOWN_ERROR} 13900042 - 内部未知错误，调用其它部件返回的除以上错误之外的其它错误。
 *         {@link E_NO_ERROR} 0 - 接口调用成功。
 * @since 12
 */
FileManagement_ErrCode OH_FileShare_DeactivatePermission(const FileShare_PolicyInfo *policies, unsigned int policyNum,
    FileShare_PolicyErrorResult **result, unsigned int *resultNum);

/**
 * @brief 校验所选择的多个文件或目录URI的持久化授权。
 *
 * @permission ohos.permission.FILE_ACCESS_PERSIST
 * @param policies 一个指向FileShare_PolicyInfo实例的指针。
 * @param policyNum FileShare_PolicyInfo实例数组的大小。
 * @param result 授权校验结果指针。请引用头文件malloc.h并使用free()进行资源释放。
 * @param resultNum 校验结果数组的大小.
 * @return 返回FileManageMent模块错误码{@link FileManagement_ErrCode}。
 *         {@link E_PARAMS} 401 - 输入参数无效。可能的原因有：1.参数policies或参数result或参数resultNum为空指针;
 *             2.参数policyNum值为0或者超过最大长度(500);3.参数policies中携带的uri为空或者length为0或者uri的长度与length不一致。
 *         {@link E_DEVICE_NOT_SUPPORT} 801 - 当前设备类型不支持此接口。
 *         {@link E_PERMISSION} 201 - 接口权限校验失败。
 *         {@link E_ENOMEM} 13900011 - 分配或者拷贝内存失败。
 *         {@link E_EPERM} 13900001 - 操作不被允许。可能的原因为policies中携带的所有uri都不符合规范或者uri转换出来的路径不存在。
 *         {@link E_UNKNOWN_ERROR} 13900042 - 内部未知错误，调用其它部件返回的除以上错误之外的其它错误。
 *         {@link E_NO_ERROR} 0 - 接口调用成功。
 * @since 12
 */
FileManagement_ErrCode OH_FileShare_CheckPersistentPermission(
    const FileShare_PolicyInfo *policies, unsigned int policyNum, bool **result, unsigned int *resultNum);

/**
 * @brief 释放FileShare_PolicyErrorResult指针指向的内存资源。
 *
 * @param errorResult 一个指向FileShare_PolicyErrorResult实例的指针。
 * @param resultNum FileShare_PolicyErrorResult实例数组的大小。
 * @since 12
 */
void OH_FileShare_ReleasePolicyErrorResult(FileShare_PolicyErrorResult *errorResult, unsigned int resultNum);
#ifdef __cplusplus
};
#endif
/** @} */
#endif // FILE_MANAGEMENT_OH_FILE_SHARE_H
