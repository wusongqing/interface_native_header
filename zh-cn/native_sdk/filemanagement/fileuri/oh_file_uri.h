/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FILE_MANAGEMENT_OH_FILE_URI_H
#define FILE_MANAGEMENT_OH_FILE_URI_H

/**
 * @addtogroup fileuri
 * @{
 *
 * @brief 文件统一资源标识符（File Uniform Resource Identifier），支持fileuri与路径path的转换、有效性校验、以及指向的变换（指向的文件或路径）。
 * 该类主要用于 URI 格式验证和 URI 转换处理，不支持媒体库类型 URI 的转换和操作。
 * 并且该类仅根据现有规范进行转换，并且不能保证转换结果将实际存在。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */

/**
 * @file oh_file_uri.h
 *
 * @brief 提供uri和路径path之间的相互转换，目录uri获取，以及URi的有效性校验的方法。
 * @library libohfileuri.so
 * @since 12
 */

#include "error_code.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 将传入的路径path转换成uri。
 *
 * @param path 表示要转换的路径。
 * @param length 表示要转换路径的字节长度。
 * @param result 表示转换后的uri, 需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因:1.参数path为空指针;2.参数result为空指针;3.输入的path长度与length不一致。
 *         {@link ERR_UNKNOWN} 13900042 - 未知错误. 转换后的uri长度为0会返回此错误。
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
FileManagement_ErrCode OH_FileUri_GetUriFromPath(const char *path, unsigned int length, char **result);

/**
 * @brief 将传入的uri转换成路径path。
 *
 * @param uri 表示要转换的uri。
 * @param length 表示要转换uri的字节长度。
 * @param result 表示转换后的路径path. 需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因:1.参数uri为空指针;2.参数result为空指针;3.输入的uri长度与length不一致。
 *         {@link ERR_UNKNOWN} 13900042 - 未知错误. 转换后的路径path长度为0会返回此错误。
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
FileManagement_ErrCode OH_FileUri_GetPathFromUri(const char *uri, unsigned int length, char **result);

/**
 * @brief 获取uri所在目录的uri，如果是文件uri则获取父目录的uri，如果是目录uri则返回本身。
 *
 * @param uri 表示要获取目录的uri的原始uri。
 * @param length  表示原始uri的字节长度。
 * @param result 表示获取到的目录URi， 需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因:1.参数uri为空指针;2.参数result为空指针;3.输入的uri长度与length不一致。
 *         {@link ERR_UNKNOWN} 13900042 - 未知错误. 获取到的目录uri长度为0会返回此错误。
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
FileManagement_ErrCode OH_FileUri_GetFullDirectoryUri(const char *uri, unsigned int length, char **result);

/**
 * @brief 校验传入的uri是否有效。
 *
 * @param uri 表示需要校验的uri。
 * @param length 需要校验uri的字节长度。
 * @return 返回 true: 传入uri是有效的uri, false: 传入的uri是无效的uri。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 12
 */
bool OH_FileUri_IsValidUri(const char *uri, unsigned int length);

/**
 * @brief 获取传入的uri的文件名称。
 *
 * @param uri 传入的uri。
 * @param length 表示传入uri的字节长度。
 * @param result 表示转换后的名称. 需要使用standard library标准库的free()方法释放申请的资源。
 * @return 返回特定的错误码值，详细信息可以查看{@link FileManagement_ErrCode}。
 *         {@link ERR_PARAMS}  401 - 输入参数无效。可能的原因:1.参数uri为空指针;2.参数result为空指针;3.输入的uri长度与length不一致;4.uri格式不正确。
 *         {@link ERR_ENOMEM}  13900011 - 分配或者拷贝内存失败。
 *         {@link ERR_OK} 0 - 接口调用成功。
 * @syscap SystemCapability.FileManagement.AppFileService
 * @since 13
 */
FileManagement_ErrCode OH_FileUri_GetFileName(const char *uri, unsigned int length, char **result);
#ifdef __cplusplus
};
#endif
#endif // FILE_MANAGEMENT_OH_FILE_URI_H
