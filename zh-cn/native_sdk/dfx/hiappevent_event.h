/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HiAppEvent
 * @{
 *
 * @brief HiAppEvent模块提供应用事件打点功能。
 *
 * 为应用程序提供事件打点功能，记录运行过程中上报的故障事件、统计事件、安全事件和用户行为事件。基于事件信息，您可以分析应用的运行状态。
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file hiappevent_event.h
 *
 * @brief 定义所有预定义事件的事件名称。
 *
 * 除了与特定应用关联的自定义事件之外，您还可以使用预定义事件进行打点。
 *
 * 示例代码:
 * <pre>
 *     ParamList list = OH_HiAppEvent_CreateParamList();
 *     OH_HiAppEvent_AddInt32Param(list, PARAM_USER_ID, 123);
 *     int res = OH_HiAppEvent_Write("user_domain", EVENT_USER_LOGIN, BEHAVIOR, list);
 *     OH_HiAppEvent_DestroyParamList(list);
 * </pre>
 *
 * @kit PerformanceAnalysisKit
 * @library libhiappevent_ndk.z.so
 * @syscap SystemCapability.HiviewDFX.HiAppEvent
 * @since 8
 * @version 1.0
 */

#ifndef HIVIEWDFX_HIAPPEVENT_EVENT_H
#define HIVIEWDFX_HIAPPEVENT_EVENT_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 用户登录事件。
 *
 * @since 8
 * @version 1.0
 */
#define EVENT_USER_LOGIN "hiappevent.user_login"

/**
 * @brief 用户登出事件。
 *
 * @since 8
 * @version 1.0
 */
#define EVENT_USER_LOGOUT "hiappevent.user_logout"

/**
 * @brief 分布式服务事件。
 *
 * @since 8
 * @version 1.0
 */
#define EVENT_DISTRIBUTED_SERVICE_START "hiappevent.distributed_service_start"

/**
 * @brief 应用崩溃事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_APP_CRASH "APP_CRASH"

/**
 * @brief 应用卡顿事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_APP_FREEZE "APP_FREEZE"

/**
 * @brief 应用加载事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_APP_LAUNCH "APP_LAUNCH"

/**
 * @brief 应用滑动卡顿事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_SCROLL_JANK "SCROLL_JANK"

/**
 * @brief 应用CPU资源占用高事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_CPU_USAGE_HIGH "CPU_USAGE_HIGH"

/**
 * @brief 应用电源使用率事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_BATTERY_USAGE "BATTERY_USAGE"

/**
 * @brief 应用资源超限事件。
 *
 * @since 12
 * @version 1.0
 */
#define EVENT_RESOURCE_OVERLIMIT "RESOURCE_OVERLIMIT"

/**
 * @brief OS作用域。
 *
 * @since 12
 * @version 1.0
 */
#define DOMAIN_OS "OS"

#ifdef __cplusplus
}
#endif
/** @} */
#endif // HIVIEWDFX_HIAPPEVENT_EVENT_H