/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HiDebug
 * @{
 *
 * @brief 提供调试代码的定义。
 *
 * 本模块函数可用于获取cpu uage、memory、heap、capture trace等。
 *
 * @since 12
 */

/**
 * @file hideug_type.h
 *
 * @brief HiDebug模块代码结构体定义。
 * @kit PerformanceAnalysisKit
 * @library libohhidebug.so
 * @syscap SystemCapability.HiviewDFX.HiProfiler.HiDebug
 * @since 12
 */

#ifndef HIVIEWDFX_HIDEBUG_TYPE_H
#define HIVIEWDFX_HIDEBUG_TYPE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 错误码定义
 *
 * @since 12
 */
typedef enum HiDebug_ErrorCode {
    /** 成功 */
    HIDEBUG_SUCCESS = 0,
    /** 无效参数，可能的原因： 1.参数传值问题 2.参数类型问题 */
    HIDEBUG_INVALID_ARGUMENT = 401,
    /** 重复采集 */
    HIDEBUG_TRACE_CAPTURED_ALREADY = 11400102,
    /** 没有写文件的权限 */
    HIDEBUG_NO_PERMISSION = 11400103,
    /** 系统内部错误。 */
    HIDEBUG_TRACE_ABNORMAL = 11400104,
    /** 当前没有trace正在运行 */
    HIDEBUG_NO_TRACE_RUNNING = 11400105
} HiDebug_ErrorCode;

/**
 * @brief 应用程序所有线程的CPU使用率结构体定义。
 *
 * @since 12
 */
typedef struct HiDebug_ThreadCpuUsage {
    /**
     * 线程ID。
     */
    uint32_t threadId;
    /**
     * 线程CPU使用率百分比。
     */
    double cpuUsage;
    /**
     * 下一个线程的使用率信息。
     */
    struct HiDebug_ThreadCpuUsage *next;
} HiDebug_ThreadCpuUsage;

/**
 * @brief HiDebug_ThreadCpuUsage指针定义。
 *
 * @since 12
 */
typedef HiDebug_ThreadCpuUsage* HiDebug_ThreadCpuUsagePtr;

/**
 * @brief 系统内存信息结构类型定义。
 *
 * @since 12
 */
typedef struct HiDebug_SystemMemInfo {
    /**
     * 系统总的内存，以KB为单位。
     */
    uint32_t totalMem;
    /**
     * 系统空闲的内存，以KB为单位。
     */
    uint32_t freeMem;
    /**
     * 系统可用的内存，以KB为单位。
     */
    uint32_t availableMem;
} HiDebug_SystemMemInfo;

/**
 * @brief 应用程序进程本机内存信息结构类型定义。
 *
 * @since 12
 */
typedef struct HiDebug_NativeMemInfo {
    /**
     * 进程比例集大小内存，以KB为单位。
     */
    uint32_t pss;
    /**
     * 虚拟内存大小，以KB为单位。
     */
    uint32_t vss;
    /**
     * 常驻集大小，以KB为单位。
     */
    uint32_t rss;
    /**
     * 共享脏内存的大小，以KB为单位。
     */
    uint32_t sharedDirty;
    /**
     * 专用脏内存的大小，以KB为单位。
     */
    uint32_t privateDirty;
    /**
     * 共享干净内存的大小，以KB为单位。
     */
    uint32_t sharedClean;
    /**
     * 专用干净内存的大小，以KB为单位。
     */
    uint32_t privateClean;
} HiDebug_NativeMemInfo;

/**
 * @brief 应用程序进程内存限制结构类型定义。
 *
 * @since 12
 */
typedef struct HiDebug_MemoryLimit {
    /**
     * 应用程序进程驻留集的限制，以KB为单位。
     */
    uint64_t rssLimit;
    /**
     * 应用程序进程的虚拟内存限制，以KB为单位。
     */
    uint64_t vssLimit;
} HiDebug_MemoryLimit;

/**
 * @brief 采集trace线程的类型。
 *
 * @since 12
 */
typedef enum HiDebug_TraceFlag {
    /** 只采集当前应用主线程 */
    HIDEBUG_TRACE_FLAG_MAIN_THREAD = 1,
    /** 采集当前应用下所有线程 */
    HIDEBUG_TRACE_FLAG_ALL_THREADS = 2
} HiDebug_TraceFlag;
#ifdef __cplusplus
}
#endif

/**
 * @brief FFRT任务标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_FFRT (1ULL << 13)
/**
 * @brief 公共库子系统标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_COMMON_LIBRARY (1ULL << 16)
/**
 * @brief HDF子系统标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_HDF (1ULL << 18)
/**
 * @brief 网络标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_NET (1ULL << 23)
/**
 * @brief NWeb标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_NWEB (1ULL << 24)
/**
 * @brief 分布式音频标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_AUDIO (1ULL << 27)
/**
 * @brief 文件管理标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_FILE_MANAGEMENT (1ULL << 29)
/**
 * @brief OHOS通用标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_OHOS (1ULL << 30)
/**
 * @brief Ability Manager标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_ABILITY_MANAGER (1ULL << 31)
/**
 * @brief 相机模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_CAMERA (1ULL << 32)
/**
 * @brief 媒体模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_MEDIA (1ULL << 33)
/**
 * @brief 图像模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_IMAGE (1ULL << 34)
/**
 * @brief 音频模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_AUDIO (1ULL << 35)
/**
 * @brief 分布式数据管理器模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_DATA (1ULL << 36)
/**
 * @brief 图形模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_GRAPHICS (1ULL << 38)
/**
 * @brief ArkUI开发框架标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_ARKUI (1ULL << 39)
/**
 * @brief 通知模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_NOTIFICATION (1ULL << 40)
/**
 * @brief MISC模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_MISC (1ULL << 41)
/**
 * @brief 多模态输入模块标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_MULTIMODAL_INPUT (1ULL << 42)
/**
 * @brief RPC标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_RPC (1ULL << 46)
/**
 * @brief JSVM虚拟机标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_ARK (1ULL << 47)
/**
 * @brief 窗口管理器标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_WINDOW_MANAGER (1ULL << 48)
/**
 * @brief 分布式屏幕标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_SCREEN (1ULL << 50)
/**
 * @brief 分布式相机标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_CAMERA (1ULL << 51)
/**
 * @brief 分布式硬件框架标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_HARDWARE_FRAMEWORK (1ULL << 52)
/**
 * @brief 全局资源管理器标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_GLOBAL_RESOURCE_MANAGER (1ULL << 53)
/**
 * @brief 分布式硬件设备管理器标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_HARDWARE_DEVICE_MANAGER (1ULL << 54)
/**
 * @brief SA标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_SAMGR (1ULL << 55)
/**
 * @brief 电源管理器标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_POWER_MANAGER (1ULL << 56)
/**
 * @brief 分布式调度程序标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_SCHEDULER (1ULL << 57)
/**
 * @brief 分布式输入标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_DISTRIBUTED_INPUT (1ULL << 59)
/**
 * @brief 蓝牙标签。
 *
 * @since 12
 */
#define HIDEBUG_TRACE_TAG_BLUETOOTH (1ULL << 60)

/** @} */

#endif // HIVIEWDFX_HIDEBUG_TYPE_H