/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AbilityRuntime
 * @{
 *
 * @brief 提供AbilityRuntime模块上下文常量的定义。
 *
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

/**
 * @file context_constant.h
 *
 * @brief 提供AbilityRuntime模块上下文常量的定义。
 *
 * @library libability_runtime.so
 * @kit AbilityKit
 * @syscap SystemCapability.Ability.AbilityRuntime.Core
 * @since 13
 */

#ifndef ABILITY_RUNTIME_CONTEXT_CONSTANT_H
#define ABILITY_RUNTIME_CONTEXT_CONSTANT_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 文件数据加密等级。
 *
 * @since 13
 */
typedef enum {
    /**
     * 设备级加密区，设备开机后可访问的数据区。
     */
    ABILITY_RUNTIME_AREA_MODE_EL1 = 0,
    /**
     * 用户级加密区，设备开机，首次输入密码后才能够访问的数据区。
     */
    ABILITY_RUNTIME_AREA_MODE_EL2 = 1,
    /**
     * 用户级加密区，不同场景的文件权限如下：
     * 已打开文件：锁屏时，可读写；解锁后，可读写。
     * 未打开文件：锁屏时，不可打开、不可读写；解锁后，可打开、可读写。
     * 创建新文件：锁屏时，可创建、可打开、可写不可读；解锁后，可创建、可打开、可读写。
     */
    ABILITY_RUNTIME_AREA_MODE_EL3 = 2,
    /**
     * 用户级加密区，不同场景的文件权限如下：
     * 已打开文件：锁屏时，FEB2.0可读写、FEB3.0不可读写；解锁后，可读写。
     * 未打开文件：锁屏时，不可打开、不可读写；解锁后，可打开、可读写。
     * 创建新文件：锁屏时，不可创建；解锁后，可创建、可打开、可读写。
     */
    ABILITY_RUNTIME_AREA_MODE_EL4 = 3,
    /**
     * 应用级加密区，不同场景的文件权限如下：
     * 已打开文件：锁屏时，可读写；解锁后，可读写。
     * 未打开文件：锁屏时，获取DataAccessLock（JS API）下可打开、可读写，否则不可打开、不可读写；解锁后，可打开、可读写。
     * 创建新文件：锁屏时，可创建、可打开、可读写；解锁后，可创建、可打开、可读写。
     */
    ABILITY_RUNTIME_AREA_MODE_EL5 = 4,
} AbilityRuntime_AreaMode;

#ifdef __cplusplus
} // extern "C"
#endif

/** @} */
#endif // ABILITY_RUNTIME_CONTEXT_CONSTANT_H
