/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_BACKGROUOND_TASK_MANAGER_TRANSIENT_TASK_TYPE_H
#define OHOS_BACKGROUOND_TASK_MANAGER_TRANSIENT_TASK_TYPE_H

/**
 * @addtogroup TransientTask
 * @{

 * @brief 提供短时任务C接口。
 * @since 13
 * @version 1.0
 */

/**
 * @file transient_task_type.h
 *
 * @brief 定义短时任务的错误码和结构体。
 *
 * @library libtransient_task.so
 * @kit BackgroundTasksKit
 * @syscap SystemCapability.ResourceSchedule.BackgroundTaskManager.TransientTask
 * @since 13
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
/**
 * @brief 定义短时任务错误码。
 * @since 13
 */
typedef enum TransientTask_ErrorCode {
    /**
     * @error 成功。
     */
    ERR_TRANSIENT_TASK_OK = 0,
    /**
     * @error 入参错误，比如参数空指针。
     */
    ERR_TRANSIENT_TASK_INVALID_PARAM = 401,
    /**
     * @error Parcel读写操作失败。
     */
    ERR_TRANSIENT_TASK_PARCEL_FAILED = 9800002,
    /**
     * @error IPC通信失败。
     */
    ERR_TRANSIENT_TASK_TRANSACTION_FAILED = 9800003,
    /**
     * @error 系统服务失败。
     */
    ERR_TRANSIENT_TASK_SYS_NOT_READY = 9800004,
    /**
     * @error 短时任务客户端信息校验失败。
     */
    ERR_TRANSIENT_TASK_CLIENT_INFO_VERIFICATION_FAILED = 9900001,
    /**
     * @error 短时任务服务端校验失败。
     */
    ERR_TRANSIENT_TASK_SERVICE_VERIFICATION_FAILED = 9900002,
} TransientTask_ErrorCode;

/**
 * @brief 定义短时任务返回信息结构体。
 *
 * @since 13
 * @version 1.0
 */
typedef struct TransientTask_DelaySuspendInfo {
    /** 短时任务请求ID */
    int32_t requestId;
    /** 剩余时间（单位：毫秒） */
    int32_t actualDelayTime;
} TransientTask_DelaySuspendInfo;

/**
 * @brief 定义短时任务超时回调类型。
 * @since 13
 */
typedef void (*TransientTask_Callback)(void);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
