/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CRYPTO_COMMON_H
#define CRYPTO_COMMON_H

/**
 * @addtogroup CryptoCommonApi
 * @{
 *
 * @brief 为应用提供算法库通用接口功能。
 *
 * @since 12
 */

/**
 * @file crypto_common.h
 *
 * @brief 定义通用API接口。
 *
 * @library libohcrypto.z.so
 * @kit Crypto Architecture Kit
 * @syscap SystemCapability.Security.CryptoFramework
 * @since 12
 */

#include <stdint.h>
#include <stddef.h>

/**
 * @brief 加解密数据结构体。
 *
 * @since 12
 */
typedef struct Crypto_DataBlob {
    /** 数据Blob的内容。 */
    uint8_t *data;
    /** 数据Blob的长度。 */
    size_t len;
} Crypto_DataBlob;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 加解密错误返回码枚举。
 *
 * @since 12
 */
typedef enum {
    /**  表示操作成功。 */
    CRYPTO_SUCCESS = 0,
    /**  输入参数不合法。 */
    CRYPTO_INVALID_PARAMS = 401,
    /**  不支持的函数或算法。 */
    CRYPTO_NOT_SUPPORT = 801,
    /**  内存错误。 */
    CRYPTO_MEMORY_ERROR = 17620001,
    /**  表示加解密操作错误。 */
    CRYPTO_OPERTION_ERROR = 17630001,
} OH_Crypto_ErrCode;

/**
 * @brief 定义加解密操作类型。
 *
 * @since 12
 */
typedef enum {
    /** 加密操作。 */
    CRYPTO_ENCRYPT_MODE = 0,
    /** 解密操作。 */
    CRYPTO_DECRYPT_MODE = 1,
} Crypto_CipherMode;

/**
 * @brief 释放dataBlob数据。
 *
 * @param dataBlob 需要释放的dataBlob数据。
 * @since 12
 */
void OH_Crypto_FreeDataBlob(Crypto_DataBlob *dataBlob);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* CRYPTO_COMMON_H */