/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CRYPTO_SYM_KEY_H
#define CRYPTO_SYM_KEY_H

#include "crypto_common.h"

/**
 * @addtogroup CryptoSymKeyApi
 * @{
 *
 * @brief 提供对称密钥相关功能接口。
 *
 * @since 12
 */

/**
 * @file crypto_sym_key.h
 *
 * @brief 定义对称密钥接口。
 *
 * @library libohcrypto.z.so
 * @kit Crypto Architecture Kit
 * @syscap SystemCapability.Security.CryptoFramework
 * @since 12
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义对称密钥结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoSymKey OH_CryptoSymKey;

/**
 * @brief 定义对称密钥生成器结构体。
 *
 * @since 12
 */
typedef struct OH_CryptoSymKeyGenerator OH_CryptoSymKeyGenerator;

/**
 * @brief 根据给定的算法名称创建对称密钥生成器。
 *
 * @param algoName 用于生成对称密钥的算法名称。例如: "AES256"。
 * @param ctx 指向对称密钥生成器实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymKeyGenerator_Create(const char *algoName, OH_CryptoSymKeyGenerator **ctx);

/**
 * @brief 随机生成对称密钥。
 *
 * @param ctx 指向对称密钥生成器实例。
 * @param keyCtx 指向对称密钥的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymKeyGenerator_Generate(OH_CryptoSymKeyGenerator *ctx, OH_CryptoSymKey **keyCtx);

/**
 * @brief 将对称密钥数据转换为对称密钥。
 *
 * @param ctx 指向对称密钥生成器实例。
 * @param keyData 指向生成对称密钥的数据。
 * @param keyCtx 指向对称密钥实例的指针。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymKeyGenerator_Convert(OH_CryptoSymKeyGenerator *ctx, const Crypto_DataBlob *keyData,
    OH_CryptoSymKey **keyCtx);

/**
 * @brief 获取对称密钥生成器的算法名称。
 *
 * @param ctx 指向对称密钥生成器实例的指针。
 * @return 返回对称密钥算法名称。
 * @since 12
 */
const char *OH_CryptoSymKeyGenerator_GetAlgoName(OH_CryptoSymKeyGenerator *ctx);

/**
 * @brief 销毁对称密钥生成器。
 *
 * @param ctx 指向对称密钥生成器实例的指针。
 * @since 12
 */
void OH_CryptoSymKeyGenerator_Destroy(OH_CryptoSymKeyGenerator *ctx);

/**
 * @brief 从对称密钥获取对称密钥算法名称。
 *
 * @param keyCtx 指向对称密钥实例。
 * @return 返回对称密钥算法名称。
 * @since 12
 */
const char *OH_CryptoSymKey_GetAlgoName(OH_CryptoSymKey *keyCtx);

/**
 * @brief 从密钥实例获取对称密钥数据。
 *
 * @param keyCtx 指向对称密钥实例。
 * @param out 获取到的结果。
 * @return {@link OH_Crypto_ErrCode}:
 *         0 - 成功。\n
 *         401 - 参数无效。\n
 *         801 - 操作不支持。\n
 *         17620001 - 内存错误。\n
 *         17630001 - 调用三方算法库API出错。
 * @since 12
 */
OH_Crypto_ErrCode OH_CryptoSymKey_GetKeyData(OH_CryptoSymKey *keyCtx, Crypto_DataBlob *out);

/**
 * @brief 销毁对称密钥实例。
 *
 * @param keyCtx 指向对称密钥实例。
 * @since 12
 */
void OH_CryptoSymKey_Destroy(OH_CryptoSymKey *keyCtx);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* CRYPTO_SYM_KEY_H */
