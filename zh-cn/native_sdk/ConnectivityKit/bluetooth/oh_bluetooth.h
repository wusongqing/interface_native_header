/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Bluetooth
 * @{
 *
 * @brief 提供用于查询蓝牙开关状态的功能。
 * @since 13
 */
/**
 * @file oh_bluetooth.h
 * @kit ConnectivityKit
 * @brief 定义查询蓝牙开关状态的接口。
 * @library libbluetooth.so
 * @syscap SystemCapability.Communication.Bluetooth.Core
 * @since 13
 */

#ifndef OH_BLUETOOTH_H
#define OH_BLUETOOTH_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义蓝牙开关状态的枚举值。
 *
 * @since 13
 */
typedef enum Bluetooth_SwitchState {
    /** 表示蓝牙关闭。 */
    BLUETOOTH_STATE_OFF = 0,
    /** 表示蓝牙打开中。 */
    BLUETOOTH_STATE_TURNING_ON = 1,
    /** 表示蓝牙已打开，使用就绪。 */
    BLUETOOTH_STATE_ON = 2,
    /** 表示蓝牙关闭中。 */
    BLUETOOTH_STATE_TURNING_OFF = 3,
    /** 表示蓝牙LE only模式打开中。 */
    BLUETOOTH_STATE_BLE_TURNING_ON = 4,
    /** 表示蓝牙处于LE only模式。 */
    BLUETOOTH_STATE_BLE_ON = 5,
    /** 表示蓝牙LE only模式关闭中。 */
    BLUETOOTH_STATE_BLE_TURNING_OFF = 6
} Bluetooth_SwitchState;

/**
 * @brief 定义蓝牙返回值的错误码。
 *
 * @since 13
 */
typedef enum Bluetooth_ResultCode {
    /**
     * 操作成功。
     */
    BLUETOOTH_SUCCESS = 0,
    /**
     * 参数错误。可能原因：1. 输入参数为空指针；2. 参数数值超出定义范围。
     */
    BLUETOOTH_INVALID_PARAM = 401,
} Bluetooth_ResultCode;

/**
 * @brief 获取蓝牙开关状态。
 *
 * @param state - 指向接收蓝牙开关状态的枚举值的指针。
 * 需要传入非空指针，否则将返回错误码。
 * 详细定义请参考{@link Bluetooth_SwitchState}。
 * @return 返回蓝牙开关状态函数的错误码。详细定义请参考{@link Bluetooth_ResultCode}。
 *     {@link BLUETOOTH_SUCCESS} 成功获取蓝牙开关状态。
 *     {@link BLUETOOTH_INVALID_PARAM} 输入参数为空指针。
 * @since 13
 */
Bluetooth_ResultCode OH_Bluetooth_GetBluetoothSwitchState(Bluetooth_SwitchState *state);
#ifdef __cplusplus
}
#endif
/** @} */
#endif // OH_BLUETOOTH_H