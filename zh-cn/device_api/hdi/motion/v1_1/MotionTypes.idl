/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Motion
 * @{
 *
 * @brief 手势识别设备驱动对硬件服务提供通用的接口能力。
 *
 * 模块提供硬件服务对手势识别驱动模块访问统一接口，服务获取驱动对象或者代理后，通过其提供的各类方法，实现使能手势识别/
 * 去使能手势识别、订阅/取消订阅手势识别数据。
 *
 * @since 4.0
 */

/**
 * @file MotionTypes.idl
 *
 * @brief 定义手势识别模块用到的数据结构，包括手势识别类型、上报的手势识别数据结构。
 *
 * 模块包路径：ohos.hdi.motion.v1_1
 *
 * @since 4.0
 * @version 1.1
 */

package ohos.hdi.motion.v1_1;

/**
 * @brief 定义运动波配置参数。
 * 
 * 运动波形配置参数包括波形频率、波形幅度、波形使用陀螺仪。
 *
 * @since 4.0
 */
struct WaveParam {
    /** 波浪频率 */
    int waveFrequency;
    /** 波幅 */
    int waveAmplitude;
    /** 波动是使用陀螺仪 */
    boolean isUseGyroscope;
};

/**
 * @brief 枚举运动类型。
 *
 * @since 4.0
 */
enum HdfMotionTypeTag {
    /** 拾取 */
    HDF_MOTION_TYPE_PICKUP = 0,
    /** 翻转 */
    HDF_MOTION_TYPE_FLIP,
    /** 贴近耳朵 */
    HDF_MOTION_CLOSE_TO_EAR,
    /** 抖动 */
    HDF_MOTION_TYPE_SHAKE,
    /** 屏幕旋转 */
    HDF_MOTION_TYPE_ROTATION,
    /** 口袋模式 */
    HDF_MOTION_TYPE_POCKET_MODE,
    /** 远离耳朵 */
    HDF_MOTION_TYPE_LEAVE_EAR,
    /** 手腕向上 */
    HDF_MOTION_TYPE_WRIST_UP,
    /** 手腕向下 */
    HDF_MOTION_TYPE_WRIST_DOWN,
    /** 波浪 */
    HDF_MOTION_TYPE_WAVE,
    /** 步进计数器 */
    HDF_MOTION_TYPE_STEP_COUNTER,
    /** 设备之间的接触 */
    HDF_MOTION_TYPE_TOUCH_LINK,
    /** 预留字段 */
    HDF_MOTION_TYPE_RESERVED,
    /** 最大运动类型 */
    HDF_MOTION_TYPE_MAX,
};
/** @} */