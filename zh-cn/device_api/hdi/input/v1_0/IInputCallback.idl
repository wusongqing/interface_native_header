/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiInput
 * @{
 *
 * @brief Input模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据Input模块提供的向上统一接口获取如下能力：Input设备的打开和关闭、Input事件获取、设备信息查询、回调函数注册、特性状态控制等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IInputCallback.idl
 *
 * @brief Input模块为Input服务提供的数据上报和热插拔事件上报的回调。
 *
 * 模块包路径：ohos.hdi.input.v1_0
 *
 * 引用：ohos.hdi.input.v1_0.InputTypes
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.input.v1_0;

import ohos.hdi.input.v1_0.InputTypes;

/**
 * @brief 定义Input模块的回调函数。
 *
 * 当设备进行数据上报、插拔等操作时，上层服务可以调用如下回调函数，处理对应的数据信息。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IInputCallback {
    /**
     * @brief 输入事件数据上报的回调函数。
     *
     * @param pkgs 驱动上报的Input事件数据，具体参考{@link EventPackage}。
     * @param devIndex Input设备索引，用于标志Input设备，取值从0开始，最多支持32个设备。
     *
     * @since 3.2
     * @version 1.0
     */
    EventPkgCallback([in] struct EventPackage[] pkgs, [in] unsigned int devIndex);

    /**
     * @brief 热插拔事件上报的回调函数。
     *
     * @param event 上报的热插拔事件数据，具体参考{@link HotPlugEvent}。
     *
     * @since 3.2
     * @version 1.0
     */
    HotPlugCallback([in] struct HotPlugEvent event);
}
/** @} */
