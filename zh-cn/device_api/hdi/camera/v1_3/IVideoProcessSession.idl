/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 5.0
 * @version 1.3
 */

/**
 * @file IVideoProcessSession.idl
 *
 * @brief 声明用于视频处理会话的API。
 *
 * 模块包路径：ohos.hdi.camera.v1_3
 *
 * 引用：
 * - ohos.hdi.camera.v1_3.Types
 * - ohos.hdi.camera.v1_1.Types
 *
 * @since 5.0
 * @version 1.0
 */

package ohos.hdi.camera.v1_3;

import ohos.hdi.camera.v1_3.Types;
import ohos.hdi.camera.v1_1.Types;

/**
 * @brief 图像处理会话进程。
 *
 * 获取待处理视频，准备需要处理视频，创建流，提交流，释放流，处理视频，删除视频，执行会话中断，会话重启。
 *
 * @since 5.0
 * @version 1.0
 */
interface IVideoProcessSession {

    /**
     * @brief 获取未处理的挂起视频的ID。
     *
     * @param videoIds 待处理视频的ID。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    GetPendingVideos([out] List<String> videoIds);

    /**
     * @brief 准备待处理的视频。
     *
     * @param videoId 待处理视频的id。
     * @param fd 待处理视频的fd。
     * @param streamDescs 返回待处理视频流信息。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    Prepare([in] String videoId, [in] FileDescriptor fd, [out] StreamDescription []streamDescs);

    /**
     * @brief 创建流。
     *
     * @param streamInfos 需要创建流信息列表，详细信息请参阅 {@link StreamInfo}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    CreateStreams([in] struct StreamInfo_V1_1[] streamInfos);

    /**
     * @brief 配置流。
     *
     * 接口调用必须在调用 {@link CreateStreams}之后。
     *
     * @param modeSetting 流的配置信息，报错帧率和zoom信息。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    CommitStreams([in] unsigned char[] modeSetting);

    /**
     * @brief 释放流。
     *
     * @param streamInfos 需要释放流的信息列表。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    ReleaseStreams([in] struct StreamInfo_V1_1[] streamInfos);

    /**
     * @brief 按照视频id处理对对应的视频。
     *
     * @param videoId 需要处理视频的id。
     * @param timestamp 需要从视频的时间戳开始处理。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    ProcessVideo([in] String videoId, [in] unsigned long timestamp);

    /**
     * @brief 通过视频id删除视频。
     *
     * @param videoId 需要删除视频id。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    RemoveVideo([in] String videoId);

    /**
     * @brief 中断会话。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 5.0
     * @version 1.0
     */
    Interrupt();

    /**
     * @brief 重启会话。
     *
     * @since 5.0
     * @version 1.0
     */
    Reset();
}
/** @} */