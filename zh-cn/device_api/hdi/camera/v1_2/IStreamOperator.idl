/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file IStreamOperator.idl
 *
 * @brief 流的操作接口。
 *
 * 模块包路径：ohos.hdi.camera.v1_2
 *
 * 引用：
 * - ohos.hdi.camera.v1_1.IStreamOperator
 * - ohos.hdi.camera.v1_2.Types
 *
 * @since 4.1
 * @version 1.2
 */
 
package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_1.IStreamOperator;
import ohos.hdi.camera.v1_2.Types;

/**
 * @brief 定义Camera设备流操作。
 *
 * 对Camera设备执行流的创建、配置与添加参数、属性获取、句柄绑定与解除、图像捕获与取消、流的转换以及流释放操作。
 *
 * 流是指从底层设备输出，经本模块内部各环节处理，最终传递到上层服务或者应用的一组数据序列。
 * 本模块支持的流的类型有预览流，录像流，拍照流等，更多类型可查看{@link StreamIntent}。
 *
 * @since 4.1
 * @version 1.2
 */
interface IStreamOperator extends ohos.hdi.camera.v1_1.IStreamOperator {

    /**
     * @brief 更新流。
     *
     * 该函数必须在 Loop CancelCaptures {@link CancelCaptures} 之后调用。
     *
     * @param streamInfos 表示流信息列表，由 {@link StreamInfo} 定义。
     * 传递的流信息可能会被更改。因此，您可以运行{@link GetStreamAttributes}来获取创建流后最新的流属性。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    UpdateStreams([in] struct StreamInfo_V1_1[] streamInfos);

    /**
     * @brief 确认捕获。
     *
     * 该函数必须在开始捕获后调用，场景处于夜景模式。
     *
     * @param captureId 要确认的流的ID。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.1
     * @version 1.2
     */
    ConfirmCapture([in] int captureId);
}
/** @} */