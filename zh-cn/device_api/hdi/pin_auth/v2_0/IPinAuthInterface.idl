/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IPinAuthInterface.idl
 *
 * @brief 定义获取口令认证驱动的执行器列表接口，用于从口令认证驱动获取执行器对象列表。
 *
 * 模块包路径：ohos.hdi.pin_auth.v2_0
 *
 * 引用：
 * - ohos.hdi.pin_auth.v2_0.IAllInOneExecutor
 * - ohos.hdi.pin_auth.v2_0.ICollector
 * - ohos.hdi.pin_auth.v2_0.IVerifier
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.pin_auth.v2_0;

import ohos.hdi.pin_auth.v2_0.IAllInOneExecutor;
import ohos.hdi.pin_auth.v2_0.ICollector;
import ohos.hdi.pin_auth.v2_0.IVerifier;

/**
 * @brief 定义获取口令认证驱动的执行器列表接口，用于从口令认证驱动获取执行器对象列表。
 *
 * @since 3.2
 * @version 1.0
 */
interface IPinAuthInterface {
    /**
     * @brief 获取执行器列表，口令认证服务进程启动进行初始化操作时通过该接口获取口令认证驱动支持的执行器列表。
     *
     * @param allInOneExecutors 标识驱动程序的全功能执行器列表{@link IAllInOneExecutor}。
     * @param verifiers 标识驱动程序的认证器列表{@link IVerifier}。
     * @param collectors 标识驱动程序的采集器列表{@link ICollector}。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 2.0
     */
    GetExecutorList([out] IAllInOneExecutor[] allInOneExecutors, [out] IVerifier[] verifiers,
        [out] ICollector[] collectors);
}
/** @} */